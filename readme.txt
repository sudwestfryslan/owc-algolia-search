=== OWC - Algolia Search ===

Requires at least: WordPress 5.6
Tested up to: WordPress 6.4
Stable tag: 2.1.3
Version: 2.1.3

## Changelog

## 2.1.3 - 2024-10-14
### Added
- Back-end search API key

### Fixed
- An exploit where the Admin API key could be abused

### Changed
- Updated dependencies

## 2.1.2 - 2024-03-26
### Changed
- Updated dependencies

## 2.1.1 - 2024-01-05
### Changed
- Updated dependencies

## 2.1.0 - 2023-12-15
### Added
- Option to exclude posts from index
- Reindexing CLI command by posttype
- Landingspage record
- Disable results footer by attribute

### Fixed
- Not displaying popular posts under certain circumstances

## 2.0.9 - 2023-03-17
### Changed
- Add arrow keys navigation
- Add normal page support

### Fixed
- Missing filter labels
- Prevent multiple initialization
- Remove shortcodes from page content when indexing
- Records exceeding max record length when encoded to json

## 2.0.8 - 2023-02-22
### Changed
- Set max record length to 7000
- Try to block internal ip addresses
- Allow setting the placeholder

## 2.0.7 - 13 october 2022
### Changed
- Allow additional variables to be passed to container abstractions
- Updated Smalot PDF parser to 2.2
- Allow facet filters to be set through a data attribute
- Notices are now set and emitted through a dedicated Notices class

### Added
- Implemented custom Record implementation for various post types 

## 2.0.6 - 17 augustus 2022
### Changed
- Dropped PHP <= 7 support
- (Re)Index process is not fully aborted when a single post is not indexable
- Use `DOMContentLoaded` event instead of jQuery' `.ready()`

### Fixed
- Potential undefined object error if the wp object is not properly loaded

## 2.0.5 - 24 februari 2022
### Changed
- Better keyboard navigation support

## 2.0.4 - 21 januari 2022
### Changed
- Algoliasearch now gets transpiled for better browser support

## 2.0.3: 14 januari 2022
### Added
- The initial search query to the Algolia search bar if one is defined.

### Changed
- The split records routine now takes the total record length into account.   
- Remove custom template for recent searches. The JS events were not properly set with custom templates. 
- Update tabbable elements more flexible by just adding .highlightable to an element  
- Remove unclickable and unaccessible magnifier icon  

### Fixed
- Issue where pressing enter without a highlighted entry would not trigger the search bar   

## 2.0.2: 7 januari 2022
### Added
- JS and CSS assets now contain a content hash in the file name
- Auto JS & CSS polyfills for older browsers
- Search results are now navigatable through keyboard

### Changed
- Assets are now built through Webpack

### Fixed
- Fix bug where no facets would cause an type error
- Fix issue with too big record sizes.

## 2.0.1: 16 december 2021
### Updated
- Various bugfixes

## 2.0.0: 10 november 2021
### Added
- JS dependencies are now handled through NPM
- JS bundling is now handled through browserify
- PSR-4 autoloading through composer
- Analytics integration with AutoComplete
- Historic search terms are now displayed and saved locally
- Popular search results are now displayed by default (on no query input)
- Keywords are considered in the search results
- More context is displayed by highlighting matches in the post content

### Changed
- Autocomplete footer does not rely on a form element anymore
- (Partial) Rewrite of PHP classes
- Autocomplete updated to version 1.4.1
- Algolia PHP client updated to 2.5
- Plugin now requires PHP 7.4

## 1.0.8: 13 mei 2020
- Toegevoegd: Bekijk alle resultaten link optie

## 1.0.7: 18 maart
- Toegevoegd: Hardcoded aria-label

## 1.0.6: 11 maart
- Bugfix: Ophalen van tekst uit een beveiligde PDF gaf een 500 error

## 1.0.5: 11 maart
- Toegevoegd: Mogelijkheid om zelf te bepalen of en hoe een record wordt gesplitst
- Verbeterd: Controlle ingebouwd om te zien of Algolia instellingen ingesteld moeten worden of niet

## 1.0.4: 10 maart
- Aangepast: autocomplete wordt nu geactiveerd door een class i.p.v. een id

## 1.0.3: 10 maart
- Toegevoegd: geavanceerde zoekmogelijkheden om in te bouwen in je thema

## 1.0.2: 10 maart
- Toegevoegd: extra filter hooks

## 1.0.1: 9 maart
- Toegevoegd: geavanceerde mogelijkheid voor het overschijven van de WP zoekfunctie

## 1.0.0: 4 maart
- Initial release