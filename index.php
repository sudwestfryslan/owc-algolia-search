<?php

/**
 * Plugin Name: OWC - Algolia Search
 * Description: With this plugin you can index your website into Algolia
 * Version: 2.1.3
 * Author: Súdwest-Fryslân
 * Author URI: https://sudwestfryslan.nl/
 * Requires at least: 5.6
 * Tested up to: 6.4
 */

require __DIR__ . '/vendor/autoload.php';

$plugin = new SudwestFryslan\Algolia\Plugin();
$plugin->setup();
$plugin->boot();
