#!/bin/bash 

mkdir -p releases

# Remove old packages
rm -rf ./releases/owc-algolia ./releases/owc-algolia.zip

# Create an optimized build of the JS app
npm run build:prod

# Copy current dir to tmp
rsync \
    -ua \
    --exclude='vendor/*' \
    --exclude='node_modules/*' \
    --exclude='releases/*' \
    . ./releases/owc-algolia/

# Remove current vendor & node_modules folder (if any) 
# and install the dependencies without dev packages.
cd ./releases/owc-algolia || exit
rm -rf ./vendor/ ./node_modules/
composer install -o --no-dev


# Remove unneeded files in a WordPress plugin
rm -rf ./.git ./composer.json ./composer.lock ./package.sh \
    ./.vscode ./workspace.code-workspace ./bitbucket-pipelines.yml \
    ./.phplint-cache ./.phpunit.result.cache ./.editorconfig ./.eslintignore \
    ./.eslintrc.json ./.gitignore ./phpunit.xml.dist ./psalm.xml ./.babelrc \
    ./package.json ./package-lock.json ./releases ./assets/src

cd ../

# Create a zip file from the optimized plugin folder
zip -rq owc-algolia.zip ./owc-algolia
rm -rf ./owc-algolia

echo "Zip completed @ $(pwd)/owc-algolia.zip"
