<?php

/**
 * Legacy
 */

function owc_algolia_is_active()
{
    return true;
}

function owc_algolia_queue_stop()
{

    // Set the cron names that need to be stopped
    $cron_names = [
        'algolia_start_reindex_from_queue',
        'algolia_reindex_from_queue',
        'algolia_update_post_from_queue'
    ];

    // Get the registerd crons
    $crons = _get_cron_array();
    if (empty($crons)) {
        return 0;
    }

    // Loop trough the timestamps
    foreach ($crons as $timestamp => $cron) {
        // Loop trough the crons
        foreach ($cron as $hook => $items) {
            if (in_array($hook, $cron_names)) {
                // Get the info
                $info = reset($items);

                // Check if the args are available
                if (isset($info['args'])) {
                    // Unschedule the event
                    wp_unschedule_event($timestamp, $hook, $info['args']);
                }
            }
        }
    }
}

function owc_algolia_has_queue_clean()
{

    // Get the registerd crons
    $crons = _get_cron_array();
    if (empty($crons)) {
        return 0;
    }

    // Loop trough the timestamps
    foreach ($crons as $timestamp => $cron) {
        // Loop trough the crons
        foreach ($cron as $hook => $items) {
            if ($hook == 'algolia_clean_by_index_id_from_queue' || $hook == 'algolia_clean_from_queue')
                return true;
        }
    }

    return false;
}

function owc_algolia_queue_reindex($time = 180)
{

    // Remove previous schedules
    wp_clear_scheduled_hook('algolia_reindex_from_queue');

    // Add new single event
    wp_schedule_single_event(time() + intval($time), 'algolia_reindex_from_queue');
}

function owc_algolia_queue_start_reindex($time = 180)
{

    // Remove previous schedules
    owc_algolia_queue_stop();

    // Add new single event
    wp_schedule_single_event(time() + intval($time), 'algolia_start_reindex_from_queue');
}

function owc_algolia_queue_clean_by_index_id($index_id, $time = 30)
{

    // Add new single event
    wp_schedule_single_event(time() + intval($time), 'algolia_clean_by_index_id_from_queue', [$index_id]);

    // Schedule a reindex
    owc_algolia_queue_start_reindex(60);
}

function owc_algolia_queue_clean($time = 30)
{

    // Add new single event
    wp_schedule_single_event(time() + intval($time), 'algolia_clean_from_queue');

    // Schedule a reindex
    owc_algolia_queue_start_reindex(60);
}

function owc_algolia_get_fake_post()
{

    // Get the post
    $posts = get_posts([
        'numberposts'       => 1,
        'post_type'         => 'algolia-fake-post',
        'post_status'       => 'any',
        'suppress_filters'  => true
    ]);

    // Return the single post
    return ($posts && isset($posts[0]) ? $posts[0] : false);
}

function owc_algolia_get_fake_post_id()
{

    // Get the fake post
    $post = owc_algolia_get_fake_post();

    // Check if the fake post is found
    if ($post)
        return $post->ID;

    // Default return false
    return false;
}

function owc_algolia_create_fake_post()
{

    // Create post object
    $post_arr = array(
        'post_title'    => '__ALGOLIA__',
        'post_content'  => '',
        'post_status'   => 'draft',
        'post_type'     => 'algolia-fake-post'
    );

    // Insert the post
    wp_insert_post($post_arr, false);
}

function owc_algolia_get_autocomplete_footer()
{

    // Check if algolia view all link is enabled
    if (get_option('algolia_front_end_view_all_link') == 1) {
        // Set the label
        $label = __('View all results', 'owc-algolia');

        // Return the link
        return sprintf('<div class="aa-suggestion aa-view-all" role="option">
          <a class="suggestion-link" href="#" title="%s">
            <div class="suggestion-post-attributes">
                <span class="suggestion-post-title">%s</span>
            </div>
          </a>
        </div>', $label, $label);
    }

    // Default return empty string
    return '';
}
