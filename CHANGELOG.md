# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.1] - 2024-01-05
### Changed
- Updated dependencies

## [2.1.0] - 2023-12-15
### Added
- Option to exclude posts from index
- Reindexing CLI command by posttype
- Landingspage record
- Disable results footer by attribute

### Fixed
- Not displaying popular posts under certain circumstances

## [2.0.9] - 2023-03-17
### Changed
- Add arrow keys navigation
- Add normal page support

### Fixed
- Missing filter labels
- Prevent multiple initialization
- Remove shortcodes from page content when indexing
- Records exceeding max record length when encoded to json

## [2.0.8] - 2023-02-22
### Changed
- Allow placeholder to be overridden
- Stop collecting insights by (crudely) blocking internal IP addresses
- Set max record length to 7000

## [2.0.7] - 2022-10-13
### Changed
- Allow additional variables to be passed to container abstractions
- Updated Smalot PDF parser to 2.2
- Allow facet filters to be set through a data attribute
- Notices are now set and emitted through a dedicated Notices class

### Added
- Implemented custom Record implementation for various post types 

## [2.0.6] - 2022-08-17
### Changed
- Dropped PHP <= 7 support
- (Re)Index process is not fully aborted when a single post is not indexable
- Use `DOMContentLoaded` event instead of jQuery' `.ready()`

### Fixed
- Potential undefined object error if the wp object is not properly loaded

## [2.0.5] - 2022-02-24
### Changed
- Better keyboard navigation support

## [2.0.4] - 2022-01-21
### Changed
- Algoliasearch now gets transpiled for better browser support

## [2.0.3] - 2022-01-14
### Added
- The initial search query to the Algolia search bar if one is defined.

### Changed
- The split records routine now takes the total record length into account.   
- Remove custom template for recent searches. The JS events were not properly set with custom templates. 
- Update tabbable elements more flexible by just adding .highlightable to an element  
- Remove unclickable and unaccessible magnifier icon  

### Fixed
- Issue where pressing enter without a highlighted entry would not trigger the search bar   

## [2.0.1] - 2022-01-07
### Added
- JS and CSS assets now contain a content hash in the file name
- Auto JS & CSS polyfills for older browsers
- Search results are now navigatable through keyboard

### Changed
- Assets are now built through Webpack

### Fixed
- Fix bug where no facets would cause an type error
- Fix issue with too big record sizes.

## [2.0.0] - 2021-11-10
### Added
- JS dependencies are now handled through NPM
- JS bundling is now handled through browserify
- PSR-4 autoloading through composer
- Analytics integration with AutoComplete
- Historic search terms are now displayed and saved locally
- Popular search results are now displayed by default (on no query input)
- Keywords are considered in the search results
- More context is displayed by highlighting matches in the post content

### Changed
- Autocomplete footer does not rely on a form element anymore
- (Partial) Rewrite of PHP classes
- Autocomplete updated to version 1.4.1
- Algolia PHP client updated to 2.5
- Plugin now requires PHP 7.4

## [1.0.8] - 2020-05-13
### Added
- Bekijk alle resultaten link optie

## [1.0.7] - 2020-03-18
### Added
- Hardcoded aria-label

## [1.0.6] - 2020-03-11
### Fixed
- Ophalen van tekst uit een beveiligde PDF gaf een 500 error

## [1.0.5] - 2020-03-11
### Added
- Mogelijkheid om zelf te bepalen of en hoe een record wordt gesplitst

### Changed
- Controle ingebouwd om te zien of Algolia instellingen ingesteld moeten worden of niet

## [1.0.4] - 2020-03-10
### Changed
- autocomplete wordt nu geactiveerd door een class i.p.v. een id

## [1.0.3] - 2020-03-10
### Added
- geavanceerde zoekmogelijkheden om in te bouwen in je thema

## [1.0.2] - 2020-03-10
### Added
- extra filter hooks

## [1.0.1] - 2020-03-09
### Added
- geavanceerde mogelijkheid voor het overschijven van de WP zoekfunctie

## [1.0.0] - 2020-03-04
### Added
- Initial release