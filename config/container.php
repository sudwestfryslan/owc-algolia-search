<?php

use SudwestFryslan\Algolia\Post;
use SudwestFryslan\Algolia\Container;
use Algolia\AlgoliaSearch\SearchClient;
use SudwestFryslan\Algolia\AssetLoader;
use Algolia\AlgoliaSearch\AnalyticsClient;
use SudwestFryslan\Algolia\Records\PdcItem;
use SudwestFryslan\Algolia\Settings\Notices;
use SudwestFryslan\Algolia\Records\OpenPubItem;
use SudwestFryslan\Algolia\Post\Keywords\PdcKeywordsStrategy;
use SudwestFryslan\Algolia\Post\Keywords\YoastKeywordsStrategy;
use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;
use SudwestFryslan\Algolia\Settings\Storage\OptionStorageDriver;
use SudwestFryslan\Algolia\Post\Keywords\OpenPubKeywordsStrategy;
use SudwestFryslan\Algolia\Settings\Storage\StorageDriverInterface;
use SudwestFryslan\Algolia\Settings\Storage\TransientStorageDriver;
use SudwestFryslan\Algolia\Post\Keywords\Resolver as KeywordsResolver;

return [
    'plugin.name'       => 'OWC - Algolia',
    'plugin.slug'       => 'owc-algolia',
    'plugin.version'    => '2.1.3',
    'plugin.file'       => dirname(__DIR__) . '/index.php',
    'plugin.path'       => dirname(__DIR__),
    'plugin.url'        => plugins_url(basename(dirname(__DIR__))),

    'setting.reindex' => function () {
        return get_option('algolia_reindex_queue', false);
    },

    'office.ips'        => [
        '185.114.197.200', '185.114.197.201', '185.114.197.202',
        '185.114.197.203', '185.114.197.204', '185.114.197.208',
        '185.114.197.209', '185.114.197.210',
    ],

    SearchClient::class => function (Container $container) {
        $settings = $container->get(SettingsManager::class);

        return SearchClient::create(
            $settings->getApplicationId(),
            $settings->getWordPressSearchApiKey() ?: $settings->getAdminApiKey() // Compatibility for < v2.1.1
        );
    },
    AnalyticsClient::class => function (Container $container) {
        $settings = $container->get(SettingsManager::class);

        return AnalyticsClient::create(
            $settings->getApplicationId(),
            $settings->getAdminApiKey()
        );
    },
    StorageDriverInterface::class => function (Container $container) {
        return $container->get(OptionStorageDriver::class);
    },
    Notices::class => function (Container $container) {
        return new Notices($container->get(TransientStorageDriver::class));
    },

    PdcKeywordsResolver::class => function (Container $container) {
        return new KeywordsResolver([
            $container->get(PdcKeywordsStrategy::class),
            $container->get(YoastKeywordsStrategy::class),
        ]);
    },
    OpenPubKeywordsResolver::class => function (Container $container) {
        return new KeywordsResolver([
            $container->get(OpenPubKeywordsStrategy::class),
            $container->get(YoastKeywordsStrategy::class),
        ]);
    },

    OpenPubItem::class => function (Container $container, Post $post) {
        return new OpenPubItem($post, $container->get(OpenPubKeywordsResolver::class));
    },
    PdcItem::class => function (Container $container, Post $post) {
        return new PdcItem($post, $container->get(PdcKeywordsResolver::class));
    },

    AssetLoader::class => function (Container $container) {
        return new AssetLoader($container->get('plugin.path'), $container->get('plugin.url'));
    }
];
