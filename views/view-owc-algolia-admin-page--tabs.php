<?php
if (!owc_algolia_is_active()) {
    return;
}

$page = (isset($_GET['page']) ? sanitize_text_field($_GET['page']) : '');
$tabs = [
    'algolia-settings'      => __('Main settings', 'owc-algolia'),
    'algolia-post_types'    => __('Post Types', 'owc-algolia'),
    'algolia-advanced'      => __('Advanced', 'owc-algolia')
];
?>

<button id="algolia-reindex--start" data-wrap=".algolia-reindex--status-wrap" class="page-title-action">
    <?php _e('Start indexing', 'owc-algolia'); ?>
</button>

<hr class="wp-header-end">

<h2 class="nav-tab-wrapper">
    <?php foreach ($tabs as $page_id => $title) {
        $class = ($page_id == $page ? 'nav-tab-active' : ''); ?>
        <a class="nav-tab <?= $class; ?>" href="<?= admin_url('admin.php?page=' . $page_id); ?>">
            <?= $title; ?>
        </a>
    <?php } ?>
</h2>

<div class="algolia-reindex--status-wrap" data-action="algolia_reindex">
    <span>0%</span>
</div>
