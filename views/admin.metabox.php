<div class="algolia_option_row">
    <input type="checkbox" id="algolia_exclude" name="_algolia_exclude" value="1" <?= $algoliaPost->isExcluded() ? 'checked' : ''; ?>>
    <label for="algolia_exclude">Uitsluiten van zoekmachine</label>
</div>