<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
    
    <?php include __DIR__ . '/view-owc-algolia-admin-page--tabs.php'; ?>

    <?php if (!empty($post_types)) { ?>
        <h3><?php _e('Post Type indexing settings', 'owc-algolia'); ?></h3>
        <form method="post" action="options.php">
            <table class="algolia-post_type--table">
                <thead>
                    <tr>
                        <th><?php _e('Post Type', 'owc-algolia'); ?></th>
                        <th><?php _e('Priority', 'owc-algolia'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($post_types as $post_type => $object) {
                            $active = (isset($values[$post_type]) ? 1 : '');
                            $prio   = (isset($values[$post_type]['prio']) ? intval($values[$post_type]['prio']) : 10);
                        ?>
                            <tr>
                                <td>
                                    <label><input type="checkbox" name="algolia_index_post_types[<?= $post_type; ?>][active]" value="1" <?php checked($active, 1, true); ?>> <?= $object->label; ?></label>
                                </td>
                                <td><input type="number" name="algolia_index_post_types[<?= $post_type; ?>][prio]" value="<?= $prio; ?>"></td>
                            </tr>
                    <?php } ?>
                </tbody>
            </table>
            
            <input type="hidden" name="action" value="save_algolia_post_type_settings">
            <?php
                settings_fields("algolia-post_types");
                submit_button();
            ?>
        </form>
    <?php } ?>
</div>
