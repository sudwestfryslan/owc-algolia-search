<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
    
    <?php include __DIR__ . '/view-owc-algolia-admin-page--tabs.php'; ?>

    <form method="post" action="options.php">
        <?php
            settings_fields("algolia-advanced");

            do_settings_sections("algolia-advanced-options");

            submit_button();
        ?>
    </form>
</div>
