# OWC Algolia Search (WP)

Information about developing is descripted below. 

## Overview

  * [Required](#markdown-header-required)
  * [Installation](#markdown-header-installation)
  * [Docker Commands](#markdown-header-docker-commands)
  * [Changelog](#markdown-header-changelog)
  
  
## Required
To develop on this project, you will need to download Composer: [Docker](getcomposer.org/) and NPM: [NPM](https://www.npmjs.com/).
Download the required PHP dependencies (`composer install`) and JS dependencies (`npm install`).

## Installation
1. Download the most recent version of the project
2. Open the Terminal(Mac OS) or Powershell(Windows)
3. Navigate to the folder where you extracted the project
4. Run the watch command to automatically build the assets (`npm run watch`)


## Changelog
Changes within this project and project releases can be logged in the [CHANGELOG.md](CHANGELOG.md).

## Front-end Autocomplete integration

Use this HTML as template when enabling the front-end autocomplete integration.

```
<div class="algolia-search" id="your-custom-identifier" data-facets="" data-placeholder="">
    <div class="algolia-search-bar"></div>
    <div class="algolia-search-results"></div>
    <button type="button" aria-label="zoeken" class="algolia-search-button" data-search-url="https://website.com/?s=">
        <span class="icon icon-magnifier"></span>
    </button>
</div>

```

The `data-facets` attribute can be used to enable certain filters (like facets) for every query. For example: `(attribute1:value OR attribute1:somevalue) AND (attribute2:value)`. View the [Algolia documentation](https://www.algolia.com/doc/api-reference/api-parameters/filters/) for more information.