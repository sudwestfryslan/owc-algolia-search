<?php

namespace SudwestFryslan\Algolia;

use WP_Post;
use RuntimeException;

class Post
{
    protected WP_Post $post;

    public function __construct(WP_Post $post)
    {
        $this->post = $post;
    }

    public function __get($name)
    {
        return $this->post->{$name};
    }

    public function __set($name, $value)
    {
        // nope
    }

    public static function fromWP_PostInstance(WP_Post $post): self
    {
        return new self($post);
    }

    public function toWP_PostInstance()
    {
        return $this->post;
    }

    public function isIndexable(): bool
    {
        return (bool) apply_filters(
            'algolia_post_allowed',
            true,
            $this->post,
            $this->post->post_type
        );
    }

    public function isExcluded(): bool
    {
        return (bool) $this->getMeta('_algolia_excluded');
    }

    public function setExcluded(bool $excluded): bool
    {
        return $this->updateMeta('_algolia_excluded', $excluded);
    }

    /**
     * Refresh the internal WP_Post instance.
     * @return \SudwestFryslan\Gutenberg\Post
     */
    public function refresh()
    {
        $this->post = get_post($this->post->ID);

        return $this;
    }
    /**
     * Update the stored post.
     * @param  array $data
     * @return bool
     * @throws \RuntimeException On WP error
     */
    protected function updatePost($data)
    {
        $update = wp_update_post(
            array_merge(['ID' => $this->post->ID], $data),
            true
        );

        if (is_wp_error($update)) {
            throw new RuntimeException("Unable to update post");
        }

        return true;
    }

    /**
     * Return the associated meta, if any.
     * @param  string $name
     * @return mixed
     */
    protected function getMeta($name)
    {
        return get_post_meta($this->post->ID, $name, true);
    }

    /**
     * Update a meta value.
     * @param  string $name
     * @param  mixed $value
     * @return bool
     */
    protected function updateMeta($name, $value)
    {
        update_post_meta($this->post->ID, $name, $value);

        return true;
    }

    public function toArray(): array
    {
        return (array) $this->post;
    }
}
