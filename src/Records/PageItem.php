<?php

namespace SudwestFryslan\Algolia\Records;

class PageItem extends Record
{
    protected string $type = 'page';

    public function toArray(): array
    {
        $record = $this->getBaseRecord();

        return $record;
    }
}
