<?php

namespace SudwestFryslan\Algolia\Records;

class OpenPubTheme extends Record
{
    protected string $type = 'openpub-theme';

    public function toArray(): array
    {
        $record = $this->getBaseRecord();

        if (empty($record['post_content'])) {
            $record['post_content'] = \html_entity_decode(
                wp_strip_all_tags($record['post_excerpt'], true)
            );
        }

        return $record;
    }
}
