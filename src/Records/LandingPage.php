<?php

namespace SudwestFryslan\Algolia\Records;

class LandingPage extends Record
{
    protected string $type = 'landing-pages';

    public function toArray(): array
    {
        $record = $this->getBaseRecord();

        if (empty($record['post_content'])) {
            $record['post_content'] = $record['post_excerpt'];
        }

        return $record;
    }
}
