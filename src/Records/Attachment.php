<?php

namespace SudwestFryslan\Algolia\Records;

use Throwable;
use SudwestFryslan\Algolia\Post\Keywords\Resolver as KeywordResolver;

class Attachment extends Record
{
    protected string $type = 'attachment';
    protected KeywordResolver $keywordResolver;

    public function toArray(): array
    {
        $mimeType = get_post_mime_type($this->post->toWP_PostInstance());

        $record = $this->getBaseRecord();
        $record['meta']['mime_type'] = $mimeType;
        $record['meta']['alt'] = get_post_meta(
            $this->post->ID,
            '_wp_attachment_image_alt',
            true
        );

        if ($mimeType === 'application/pdf') {
            $record['post_content'] = $this->getPdfText($this->post->ID);
        }

        return apply_filters('algolia_attachment_to_record_filtered', $record, $this->post);
    }

    public function getPdfText(): string
    {
        $attachment = get_attached_file($this->post->ID);

        if (! $attachment || ! file_exists($attachment)) {
            return '';
        }

        try {
            $parser = new \Smalot\PdfParser\Parser();
            $pdf = $parser->parseFile($attachment);

            $text = trim(preg_replace('/\t+/', '', $pdf->getText()));
            $text = preg_replace('/\s+/', ' ', $text);

            return sanitize_text_field($text);
        } catch (Throwable $e) {
            return '';
        }
    }

    protected function getImages(): array
    {
        $images = [];
        $thumbnailId = (int) $this->post->ID;

        $sizes = (array) apply_filters('algolia_post_images_sizes', ['thumbnail']);
        foreach ($sizes as $size) {
            $images[$size] = $this->getImageInfo($thumbnailId, $size);
        }

        return (array) apply_filters('algolia_getImages', array_filter($images));
    }
}
