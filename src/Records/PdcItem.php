<?php

namespace SudwestFryslan\Algolia\Records;

use WP_Query;
use SudwestFryslan\Algolia\Post;
use SudwestFryslan\Algolia\Post\Keywords\Resolver as KeywordResolver;

class PdcItem extends Record
{
    protected string $type = 'pdc-item';
    protected KeywordResolver $keywordResolver;

    public function __construct(Post $post, KeywordResolver $keywordResolver)
    {
        parent::__construct($post);
        $this->keywordResolver = $keywordResolver;
    }

    public function toArray(): array
    {
        $record = $this->getBaseRecord();
        $record['themes'] = $this->getConnectedThemes();
        $record['keywords'] = implode(', ', $this->getKeywords());

        return $record;
    }

    protected function getConnectedThemes(): array
    {
        $connected = new WP_Query([
            'connected_type'    => 'pdc-item_to_pdc-category',
            'connected_items'   => $this->post->ID,
            'nopaging'          => true,
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
        ]);

        if (empty($connected->posts)) {
            return [];
        }

        return array_map(function ($item) {
            return apply_filters('the_title', $item->post_title);
        }, $connected->posts);
    }

    protected function getKeywords(): array
    {
        $items = array_map('trim', array_unique(
            $this->keywordResolver->execute($this->post)
        ));

        return array_filter($items);
    }
}
