<?php

namespace SudwestFryslan\Algolia\Records;

use InvalidArgumentException;
use SudwestFryslan\Algolia\Post;

abstract class Record
{
    protected Post $post;
    protected string $type = 'post';

    public function __construct(Post $post)
    {
        if ($post->post_type !== $this->type) {
            throw new InvalidArgumentException(
                "Cannot create record %s with post of type %s",
                self::class,
                $post->post_type
            );
        }

        $this->post = $post;
    }

    public function getBaseRecord(): array
    {
        $post = $this->post->toWP_PostInstance();

        $record = [
            'post_id'               => $post->ID,
            'post_type'             => $post->post_type,
            'post_title'            => apply_filters('the_title', $post->post_title),
            'post_content'          => $this->getPostContent(),
            'post_excerpt'          => apply_filters('the_excerpt', $post->post_excerpt),
            'post_date'             => get_post_time('U', false, $post),
            'post_date_formatted'   => get_the_date('', $post),
            'post_modified'         => get_post_modified_time('U', false, $post),
            'comment_count'         => (int) $post->comment_count,
            'menu_order'            => (int) $post->menu_order,
            'taxonomies'            => $this->getTaxonomies(),
            'images'                => $this->getImages(),
            'permalink'             => get_permalink($post),
            'is_sticky'             => (int) is_sticky($post->ID),
            'meta'                  => [],
        ];

        return apply_filters('algolia_post_to_record_filtered', $record, $this->post);
    }

    protected function getPostContent(): string
    {
        $content = apply_filters('the_content', $this->post->post_content);
        $content = wp_strip_all_tags($content, true);
        $content = strip_shortcodes($content);

        return html_entity_decode($content);
    }

    protected function getTaxonomies(): array
    {
        $results = [];
        $taxonomies = get_object_taxonomies($this->post->post_type, 'objects');

        if (! is_array($taxonomies)) {
            return [];
        }

        foreach ($taxonomies as $taxonomy) {
            $terms = wp_get_object_terms($this->post->ID, $taxonomy->name);
            $terms = is_array($terms) ? $terms : array();

            $values = wp_list_pluck($terms, 'name');
            if (! empty($values)) {
                $results[$taxonomy->name] = $values;
            }
        }

        return $results;
    }

    protected function getImages(): array
    {
        $images = [];
        $thumbnailId = (int) get_post_thumbnail_id($this->post->ID);

        if (! $thumbnailId) {
            return (array) apply_filters('algolia_getImages', $images);
        }

        $sizes = (array) apply_filters('algolia_post_images_sizes', ['thumbnail']);
        foreach ($sizes as $size) {
            $images[$size] = $this->getImageInfo($thumbnailId, $size);
        }

        return (array) apply_filters('algolia_getImages', $images);
    }

    protected function getImageInfo(int $imageId, string $size): array
    {
        $info = wp_get_attachment_image_src($imageId, $size);
        if (! is_array($info) || count($info) < 3) {
            return [];
        }

        [$url, $width, $height] = $info;

        return compact('url', 'width', 'height');
    }

    abstract public function toArray(): array;
}
