<?php

namespace SudwestFryslan\Algolia\Records;

class ConstructionProject extends Record
{
    protected string $type = 'construc_projects';

    public function toArray(): array
    {
        $record = $this->getBaseRecord();

        return $record;
    }
}
