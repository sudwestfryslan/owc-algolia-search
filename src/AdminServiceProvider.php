<?php

namespace SudwestFryslan\Algolia;

use SudwestFryslan\Algolia\Settings\Notices;

class AdminServiceProvider extends ServiceProvider
{
    protected AssetLoader $assetLoader;

    public function __construct(Container $container, AssetLoader $loader)
    {
        parent::__construct($container);

        $this->assetLoader = $loader;
    }

    public function register(): void
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 10);
        add_action('admin_notices', [$this, 'notices'], 10);
    }

    public function createFakePost(): void
    {
        $post = owc_algolia_get_fake_post();

        if (!$post) {
            owc_algolia_create_fake_post();
        }
    }

    public function enqueueScripts(): void
    {
        wp_enqueue_style(
            'owc_algolia',
            $this->assetLoader->getUrl('css/admin.css'),
            array(),
            $this->container->get('plugin.version')
        );

        wp_enqueue_script(
            'owc_algolia',
            $this->assetLoader->getUrl('js/admin.js'),
            array('jquery'),
            $this->container->get('plugin.version'),
            true
        );

        wp_localize_script(
            'owc_algolia',
            'algolia',
            array( 'ajax_url' => admin_url('admin-ajax.php') )
        );
    }

    public function notices()
    {
        $notices = $this->container->get(Notices::class)->get();

        if (empty($notices)) {
            return;
        }

        foreach ($notices as $notice) {
            printf(
                '<div class="notice notice-%s is-dismissible"><p>%s</p></div>',
                \esc_attr($notice['type'] ?? 'error'),
                \sanitize_text_field($notice['text'] ?? '')
            );
        }
    }
}
