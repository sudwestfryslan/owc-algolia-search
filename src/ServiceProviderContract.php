<?php

namespace SudwestFryslan\Algolia;

interface ServiceProviderContract
{
    public function boot(): void;
    public function register(): void;
}
