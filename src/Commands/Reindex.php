<?php

namespace SudwestFryslan\Algolia\Commands;

use WP_CLI;
use Closure;
use SudwestFryslan\Algolia\Post;
use SudwestFryslan\Algolia\AlgoliaSearch;
use SudwestFryslan\Algolia\IndexablePostsQuery;
use Algolia\AlgoliaSearch\Response\IndexingResponse;
use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;

class Reindex
{
    protected AlgoliaSearch $search;
    protected SettingsManager $settings;
    protected ?Closure $recordSplitter;

    protected int $postsCount = 0;

    public function __construct(SettingsManager $manager, AlgoliaSearch $search)
    {
        $this->settings = $manager;
        $this->search = $search;
    }

    // wp algolia reindex [--post=123]
    public function __invoke($args, $assocArgs)
    {
        $postId = $assocArgs['post'] ?? false;
        if ($postId) {
            return WP_CLI::halt((int) $this->reindexIndividualPost($postId));
        }

        $postType = $assocArgs['post_type'] ?? false;
        if ($postType) {
            return WP_CLI::halt((int) $this->reindexPostType(sanitize_text_field($postType)));
        }

        return $this->reindexAllPosts();
    }

    public function setRecordSplitter(Closure $splitter)
    {
        $this->recordSplitter = $splitter;

        return $this;
    }

    public function getRecordSplitter(): Closure
    {
        return $this->recordSplitter ?: function ($post) {
            return [$post->toWP_PostInstance()];
        };
    }

    public function clearIndexByPosttype(string $posttype): IndexingResponse
    {
        WP_CLI::log(\sprintf('Clearing index by posttype "%s"...', $posttype));

        $index = $this->search->getIndex();

        return $index->deleteBy(['filters' => 'post_type:' . $posttype])->wait();
    }

    /**
     * Reindex a single given post
     * @param int $post
     */
    protected function reindexIndividualPost($post): bool
    {
        WP_CLI::log(\sprintf('Reindexing single post "%s"', $post));

        try {
            $post = new Post(get_post($post));
        } catch (Throwable $e) {
            WP_CLI::error(\sprintf('Unable to find post "%s"', \var_export($post, true)), false);

            return false;
        }

        $records = $this->getRecordsFromPost($post);

        if (empty($records)) {
            WP_CLI::success('No valid records created from posts, aborting.');

            return true;
        }

        WP_CLI::log(\sprintf('Inserting %d records into Algolia...', \count($records)));

        $this->search->getIndex()->saveObjects($records);

        WP_CLI::success('Woei! Done!');

        return true;
    }

    /**
     * Reindex all records by their post type.
     * @param  string $postType
     * @return bool
     */
    protected function reindexPostType(string $postType): bool
    {
        $indexablePosts = $this->getIndexablePostQuery()->ofType($postType)->execute();

        $this->postsCount = \count($indexablePosts);

        WP_CLI::log(\sprintf('Query OK. Found %d posts of type %s.', $this->postsCount, $postType));

        if (empty($indexablePosts)) {
            WP_CLI::success('No indexable posts found, aborting.');

            return true;
        }

        $this->clearIndexByPosttype($postType);

        $prepared = $this->prepareIndexablePosts($indexablePosts);

        if (empty($prepared)) {
            WP_CLI::success('No valid records created from posts, aborting.');

            return true;
        }

        WP_CLI::log(\sprintf('Inserting %d records into Algolia...', \count($prepared)));

        $this->search->getIndex()->saveObjects($prepared);

        WP_CLI::success('Woei! Done!');

        return true;
    }

    /**
     * Reindex all indexable posts. Clears the current index.
     * @return bool
     */
    protected function reindexAllPosts(): bool
    {
        WP_CLI::log('Warning: this action will clear all records from the current index.');
        WP_CLI::confirm('Are you sure you want to reindex ALL posts?');

        // Legacy
        owc_algolia_queue_stop();

        $this->clearCurrentIndex();

        $indexablePosts = $this->getIndexablePostQuery()->execute();
        $this->postsCount = \count($indexablePosts);

        WP_CLI::log(\sprintf('Query OK. Found %d posts.', $this->postsCount));

        if (empty($indexablePosts)) {
            WP_CLI::success('No indexable posts found, aborting.');

            return false;
        }

        $prepared = $this->prepareIndexablePosts($indexablePosts);

        if (empty($prepared)) {
            WP_CLI::success('No valid records created from posts, aborting.');

            return true;
        }

        WP_CLI::log(\sprintf('Inserting %d records into Algolia...', \count($prepared)));

        $this->search->getIndex()->saveObjects($prepared);

        WP_CLI::success('Woei! Done!');

        return true;
    }

    protected function prepareIndexablePosts(array $indexablePosts)
    {
        $prepared = [];
        foreach ($indexablePosts as $iterator => $indexablePost) {
            $this->outputProgress($indexablePost, ($iterator + 1));

            $records = $this->getRecordsFromPost($indexablePost);

            $prepared = \array_merge($prepared, $records);
        }

        return $prepared;
    }

    protected function getRecordsFromPost(Post $post): array
    {
        if ($post->isIndexable()) {
            return \call_user_func($this->getRecordSplitter(), $post);
        }

        WP_CLI::warning(\sprintf(
            'Post "%s" is not indexable.',
            $this->getShortenedPostTitle($post->post_title)
        ));

        return [];
    }

    protected function getShortenedPostTitle($postTitle)
    {
        return \substr($postTitle, 0, 48) . (\mb_strlen($postTitle) > 48 ? '...' : '');
    }

    protected function clearCurrentIndex(): IndexingResponse
    {
        WP_CLI::log('Clearing current index...');

        $index = $this->search->getIndex();
        $indexId = $this->settings->getIndexIdentifier();

        if (! $indexId || empty($indexId)) {
            return $index->clearObjects()->wait();
        }

        return $index->deleteBy(['filters' => 'indexID:' . $indexId])->wait();
    }

    /**
     * Return a queried list of posts that are converted to Gutenberg. These
     * posts should also contain a back-up of the Visual Composer content.
     */
    protected function getIndexablePostQuery(): IndexablePostsQuery
    {
        WP_CLI::log('Fetching all indexable posts...');

        return new IndexablePostsQuery($this->settings);
    }

    protected function outputProgress($post, $number)
    {
        return WP_CLI::log(\sprintf(
            '[%d/%d] Preparing %s',
            $number,
            $this->postsCount,
            $this->getShortenedPostTitle($post->post_title)
        ));
    }
}
