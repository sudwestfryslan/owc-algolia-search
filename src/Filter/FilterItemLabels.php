<?php

namespace SudwestFryslan\Algolia\Filter;

class FilterItemLabels
{
    protected array $labels = [
        'all'           => 'Toon alles',
        'post'          => 'Nieuws',
        'page'          => 'Pagina',
        'pdc-item'      => 'Informatie',
        'attachment'    => 'Documenten',
        'openpub-item'  => 'Nieuws',
        'pdc-category'  => 'Thema',
        'openpub-theme' => 'Nieuws thema'
    ];

    public function get(string $name): ?string
    {
        return $this->labels[$name] ?? $name;
    }
}
