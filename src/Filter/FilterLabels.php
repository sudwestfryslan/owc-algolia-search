<?php

namespace SudwestFryslan\Algolia\Filter;

class FilterLabels
{
    protected array $labels = [
        'post_type'     => 'Typen',
        'themes'        => 'Thema\'s',
    ];

    public function get(string $name): ?string
    {
        return $this->labels[$name] ?? $name;
    }
}
