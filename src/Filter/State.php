<?php

namespace SudwestFryslan\Algolia\Filter;

use SudwestFryslan\Algolia\Filter\Filter;
use SudwestFryslan\Algolia\Filter\FilterItem;

class State
{
    protected int $resultCount;
    protected array $filters = [];
    protected FilterLabels $filterLabels;
    protected FilterItemLabels $filterItemLabels;

    public function __construct()
    {
        $this->filterLabels = new FilterLabels(); // Move to DI
        $this->filterItemLabels = new FilterItemLabels(); // Move to DI
    }

    public function has(string $name): bool
    {
        return isset($this->filters[$name]);
    }

    public function get(string $name): ?Filter
    {
        return $this->filters[$name] ?? null;
    }

    public function all(): array
    {
        return $this->filters;
    }

    public function getResultCount()
    {
        return $this->resultCount;
    }

    public function setResultCount(int $count): State
    {
        $this->resultCount = $count;

        return $this;
    }

    public function setMultiple(array $filters): State
    {
        foreach ($filters as $name => $items) {
            $this->add($name, $items);
        }

        return $this;
    }

    public function add(string $name, array $items): State
    {
        $items = array_map([$this, 'morphToFilterItem'], array_keys($items), array_values($items));

        $filter = new Filter($name, $this->filterLabels->get($name), $items);

        foreach ($filter->items() as $item) {
            $item->setParent($filter);
        }

        $this->filters[$name] = $filter;

        return $this;
    }

    public function morphToFilterItem(string $name, int $count): FilterItem
    {
        return new FilterItem($name, $this->filterItemLabels->get($name), $count);
    }

    public function getCurrentUrl(): string
    {
        $url = get_pagenum_link(1, false);
        $filters = $this->getCurrentFilters();

        foreach ($filters as $name => $value) {
            $url = add_query_arg([$name => $value], $url);
        }

        return $url;
    }

    public function hasActiveFilters()
    {
        $filters = array_filter(array_map(function ($filter) {
            return $filter->isActive();
        }, $this->filters));

        return ! empty($filters);
    }

    public function getCurrentFilters(): array
    {
        $filters = [];

        foreach ($this->filters as $filter) {
            if ($filter->isActive() === false) {
                continue;
            }

            $filters[$filter->name()] = $filter->getActiveItem()->name();
        }

        return $filters;
    }

    public function getFilterUrl(FilterItem $item): string
    {
        return $this->buildFilterUrl($item->getParentName(), $item->name());
    }

    public function getResetUrl(Filter $filter): string
    {
        return remove_query_arg($filter->name(), $this->getCurrentUrl());
    }

    public function buildFilterUrl(string $name, string $value): string
    {
        return add_query_arg([$name => $value], $this->getCurrentUrl());
    }
}
