<?php

namespace SudwestFryslan\Algolia\Filter;

class FilterItem
{
    protected string $name;
    protected string $label;
    protected ?int $count = null;
    protected ?Filter $parent;
    protected bool $active = false;

    public function __construct(string $name, string $label, ?int $count = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->count = $count;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function label(): string
    {
        return $this->label;
    }

    public function setActive(): FilterItem
    {
        $this->active = true;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setCount(int $count): FilterItem
    {
        $this->count = $count;

        return $this;
    }

    public function count(): ?int
    {
        return $this->count;
    }

    public function setParent(Filter $parent)
    {
        $this->parent = $parent;
    }

    public function getParentName(): ?string
    {
        return $this->parent ? $this->parent->name() : null;
    }

    public function parent(): ?Filter
    {
        return $this->parent;
    }

    public function belongsTo(): ?Filter
    {
        return $this->parent();
    }
}
