<?php

namespace SudwestFryslan\Algolia\Filter;

class Filter
{
    protected string $name;
    protected string $label;
    protected array $items;
    protected bool $active = false;

    public function __construct(string $name, string $label, array $items)
    {
        $this->name = $name;
        $this->items = $items;
        $this->label = $label;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function label(): string
    {
        return $this->label;
    }

    public function items(): array
    {
        return $this->items;
    }

    public function itemWithName(string $name): ?FilterItem
    {
        $items = array_filter($this->items(), function ($item) use ($name) {
            return $item->name() === $name;
        });

        return empty($items) ? null : reset($items);
    }

    public function getActiveItem(): ?FilterItem
    {
        $items = array_filter($this->items(), function ($item) {
            return $item->isActive();
        });

        return empty($items) ? null : reset($items);
    }

    public function setActive(): Filter
    {
        $this->active = true;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }
}
