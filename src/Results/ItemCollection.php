<?php

declare(strict_types=1);

namespace SudwestFryslan\Algolia\Results;

use Closure;
use SudwestFryslan\Algolia\Collection\Collection;

class ItemCollection extends Collection
{
    public function __construct(array $data)
    {
        $data = array_filter($data, function ($item) {
            return $item instanceof Item;
        });

        parent::__construct($data);
    }
}
