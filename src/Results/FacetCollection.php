<?php

declare(strict_types=1);

namespace SudwestFryslan\Algolia\Results;

use Closure;
use SudwestFryslan\Algolia\Collection\Collection;

class FacetCollection extends Collection
{
    public function __construct(array $data)
    {
        $data = array_filter($data, function ($item) {
            return $item instanceof FacetItem;
        });

        parent::__construct($data);
    }
}
