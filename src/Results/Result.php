<?php

declare(strict_types=1);

namespace SudwestFryslan\Algolia\Results;

class Result
{
    protected array $data;
    protected array $facets = [];
    protected ItemCollection $items;

    public function __construct(array $data)
    {
        $this->hydrate($data);
    }

    protected function hydrate(array $data)
    {
        $this->items = $this->morphToItemCollection($data['hits'] ?? []);

        foreach (($data['facets'] ?? []) as $name => $items) {
            $facet = new Facet($name, $this->morphToFacetCollection($items));
            $facet->items()->map(function ($item) use ($facet) {
                $item->setParent($facet);
            });

            $this->facets[] = $facet;
        }

        $this->data = $data;
    }

    public function get($name, $default = null)
    {
        return $this->data[$name] ?? $default;
    }

    public function getHitsCount(): int
    {
        return $this->get('nbHits', 0);
    }

    public function getItems(): ItemCollection
    {
        return $this->items;
    }

    public function getFacets(): array
    {
        return $this->facets;
    }

    public function hasFacets(): bool
    {
        return ! empty($this->facets);
    }

    protected function morphToItemCollection(array $items)
    {
        return new ItemCollection(array_map(function ($item) {
            return new Item($item);
        }, $items));
    }

    protected function morphToFacetCollection(array $items): FacetCollection
    {
        return new FacetCollection(array_map(function ($name, $count) {
            return new FacetItem($name, $count);
        }, array_keys($items), array_values($items)));
    }
}
