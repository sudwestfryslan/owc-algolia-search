<?php

namespace SudwestFryslan\Algolia\Results;

class Facet
{
    protected string $name;
    protected FacetCollection $items;

    public function __construct(string $name, FacetCollection $items)
    {
        $this->name = $name;
        $this->items = $items;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function items(): FacetCollection
    {
        return $this->items;
    }
}
