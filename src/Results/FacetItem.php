<?php

namespace SudwestFryslan\Algolia\Results;

class FacetItem
{
    protected string $name;
    protected int $count;
    protected ?Facet $parent;

    public function __construct(string $name, int $count, ?Facet $parent = null)
    {
        $this->name = $name;
        $this->count = $count;
        $this->parent = $parent ?: null;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function count(): int
    {
        return $this->count;
    }

    public function setParent(Facet $parent)
    {
        $this->parent = $parent;
    }

    public function getParentName(): ?string
    {
        return $this->parent ? $this->parent->name() : null;
    }

    public function parent(): ?Facet
    {
        return $this->parent;
    }

    public function belongsTo(): ?Facet
    {
        return $this->parent();
    }
}
