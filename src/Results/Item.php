<?php

namespace SudwestFryslan\Algolia\Results;

class Item
{
    protected array $result;

    public function __construct(array $result)
    {
        $this->result = $result;
    }

    public function getPostId()
    {
        return $this->getValue('post_id', 0);
    }
    public function getTitle()
    {
        return $this->hasHighlight('post_title') ?
            $this->getHighlight('post_title') :
            $this->getValue('post_title');
    }
    public function getContent()
    {
        return $this->hasSnippet('post_content') ?
            $this->getSnippet('post_content') :
            $this->getValue('post_content');
    }

    public function getKeywords()
    {
        return (string) $this->getHighlight('keywords');
    }

    public function getPermalink()
    {
        return $this->getValue('permalink');
    }

    public function getThemes(): array
    {
        return $this->getValue('themes', []);
    }

    public function getType()
    {
        return $this->getValue('post_type');
    }

    public function getHighlight(string $name, $default = null)
    {
        return $this->hasHighlight($name) ?
            $this->result['_highlightResult'][$name]['value'] :
            $default;
    }

    public function getSnippet(string $name, $default = null)
    {
        return $this->hasSnippet($name) ?
            $this->result['_snippetResult'][$name]['value'] :
            $default;
    }

    public function hasHighlight(string $name): bool
    {
        return isset($this->result['_highlightResult']) &&
            isset($this->result['_highlightResult'][$name]);
    }

    public function hasSnippet(string $name): bool
    {
        return isset($this->result['_snippetResult']) &&
            isset($this->result['_snippetResult'][$name]);
    }

    protected function getValue(string $name, $default = null)
    {
        return $this->result[$name] ?? $default;
    }
}
