<?php

namespace SudwestFryslan\Algolia;

use WP_Query;
use SudwestFryslan\Algolia\Collection\Collection;
use Algolia\AlgoliaSearch\Exceptions\AlgoliaException;
use SudwestFryslan\Algolia\Analytics\TopHitsPostResolver;

class SearchPageServiceProvider extends ServiceProvider
{
    protected Settings\Manager $settings;

    public function __construct(Container $container, Settings\Manager $manager)
    {
        parent::__construct($container);

        $this->settings = $manager;
    }

    public function register(): void
    {
        if ($this->settings->frontendSearchEnabled()) {
            add_action('pre_get_posts', [$this, 'filterSearchResults'], 10, 1);
        }
    }

    /**
     * We force the WP_Query to only return records according to Algolia's ranking.
     * @param WP_Query $query
     */
    public function filterSearchResults(WP_Query $query)
    {
        if (! $this->isSearchQuery($query)) {
            return;
        }

        global $algoliaSearchResults, $algoliaFilters;

        $searchConfig = $this->getSearchConfig(); // Move to DI?
        $searchParameters = $this->getSearchParameters($searchConfig); // Move to DI

        try {
            $service = $this->container->get(AlgoliaSearch::class);
            $index = $service->getIndex();

            $results = $index->search(
                $query->query['s'],
                $searchParameters,
                $searchConfig->get('order_by'),
                $searchConfig->get('order')
            );
        } catch (AlgoliaException $exception) {
            return error_log($exception->getMessage());
        }

        $algoliaSearchResults = new Results\Result($results);

        $algoliaFilters = new Filter\State();
        $algoliaFilters->setMultiple($algoliaSearchResults->get('facets', []));
        $algoliaFilters->setResultCount($algoliaSearchResults->getHitsCount());
        $this->setActiveFilters($algoliaFilters, $searchParameters);

        // if ($this->searchHasActiveFilters($searchParameters)) {
        //     $facetParameters = $this->getFacetSearchParameters($searchParameters);

        //     try {
        //         $filter_results = $index->search(
        //             $query->query['s'],
        //             $facetParameters,
        //             $searchConfig->get('order_by'),
        //             $searchConfig->get('order')
        //         );
        //     } catch (AlgoliaException $exception) {
        //         error_log($exception->getMessage());

        //         return;
        //     }

        //     $algoliaFilters->setMultiple($filter_results['facets']);
        //     $algoliaFilters->setResultCount($filter_results['nbHits']);

        //     $algolia_all_posts  = $filter_results['nbHits'];

        //     $this->setActiveFilters($algoliaFilters, $searchParameters);
        // }

        // Set query args
        $query->set('posts_per_page', $searchConfig->get('posts_per_page'));
        $query->set('offset', 0);
        $query->set('post_type', $this->getQueryPostType());
        $query->set('post__in', $this->getQueryWordPressIds($algoliaSearchResults));
        $query->set('orderby', 'post__in');

        // Todo: this actually still excludes trash and auto-drafts.
        $query->set('post_status', 'any');

        // Add the filters
        add_filter('found_posts', [$this, 'injectFoundPostsCount'], 10, 2);
        add_filter('posts_search', [$this, 'removeSqlQuery'], 10, 2);
    }

    protected function setActiveFilters(Filter\State $filter, array $parameters)
    {
        foreach ($parameters['facetFilters'] as $format) {
            [$name, $value] = explode(':', $format);

            if ($filter->has($name) == false) {
                continue;
            }

            $activeFilter = $filter->get($name);
            $activeFilter->setActive(true);

            if ($activeItem = $activeFilter->itemWithName($value)) {
                $activeItem->setActive(true);
            }
        }
    }


    protected function searchHasActiveFilters(array $searchParameters): bool
    {
        return isset($searchParameters['facetFilters']) &&
            apply_filters('algolia_ignore_filters_for_filter_count', false);
    }

    protected function getFacetSearchParameters(array $searchParams): array
    {
        $searchParams['attributesToRetrieve']  = ['objectID'];
        $searchParams['attributesToHighlight'] = ['objectID'];
        $searchParams['hitsPerPage'] = 1;

        unset($searchParams['facetFilters']);

        return $searchParams;
    }

    protected function getQueryWordPressIds(Results\Result $results): array
    {
        if (apply_filters('algolia_use_advanced_search_pre_get_posts', false)) {
            return [owc_algolia_get_fake_post_id() ?: 0];
        }

        return $results->getItems()->map(function ($item) {
            return $item->getPostId();
        })->toArray();
    }

    protected function getQueryPostType(): string
    {
        if (apply_filters('algolia_use_advanced_search_pre_get_posts', false)) {
            return 'algolia-fake-post';
        }

        return 'any';
    }

    protected function getSearchConfig(): Collection
    {
        $posts_per_page = (int) get_option('posts_per_page');

        return new Collection([
            'current_page'      => get_query_var('paged', get_query_var('page', false) ?: 1) ?: 1,
            'posts_per_page'    => apply_filters('algolia_search_posts_per_page', $posts_per_page),
            'order'             => apply_filters('algolia_search_order', 'desc'),
            'order_by'          => apply_filters('algolia_search_order_by', null),
        ]);
    }

    protected function getSearchParameters(Collection $config): array
    {
        return (array) apply_filters(
            'algolia_search_params',
            [
                'attributesToRetrieve'  => ['post_id', 'post_title', 'post_content', 'permalink', 'post_type'],
                'facetingAfterDistinct' => true,
                'hitsPerPage'           => $config->get('posts_per_page', 10),
                // Algolia pages are zero indexed.
                'page'                  => $config->get('current_page', 1) - 1,
                'highlightPreTag'       => '<mark class="suggestion-highlight">',
                'highlightPostTag'      => "</mark>",
                'attributesToSnippet'   => ['post_content:45', 'keywords:5'],
                'snippetEllipsisText'   => '…',
                'facetFilters'          => [],
            ]
        );
    }

    protected function isSearchQuery(WP_Query $query): bool
    {
        return ! $query->is_admin && $query->is_search() && $query->is_main_query();
    }

    public function injectFoundPostsCount($foundPosts, WP_Query $query)
    {
        global $algoliaSearchResults;

        return $this->isSearchQuery($query) && !empty($algoliaSearchResults) ?
            $algoliaSearchResults->getHitsCount() :
            $foundPosts;
    }

    /**
     * Filter the WHERE clause of the search SQL query. We'll remove the
     * where part as we consider Algolia as being the source of truth.
     * @param string   $search
     * @param WP_Query $query
     * @return string
     */
    public function removeSqlQuery($search, WP_Query $query)
    {
        return $this->isSearchQuery($query) ? '' : $search;
    }
}
