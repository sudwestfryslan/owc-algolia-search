<?php

namespace SudwestFryslan\Algolia;

use Throwable;
use Algolia\AlgoliaSearch\SearchIndex;
use Algolia\AlgoliaSearch\AnalyticsClient;
use SudwestFryslan\Algolia\Analytics\TopHitsResponse;
use Algolia\AlgoliaSearch\RequestOptions\RequestOptions;
use SudwestFryslan\Algolia\Analytics\TopSearchesResponse;
use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;

class AlgoliaAnalytics
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const ANALYTICS_HOST = 'analytics.de.algolia.com';

    protected AnalyticsClient $client;
    protected SettingsManager $settings;

    public function __construct(AnalyticsClient $client, SettingsManager $settings)
    {
        $this->client = $client;
        $this->settings = $settings;
    }

    public function getTopHits(string $index, array $parameters = []): TopHitsResponse
    {
        $options = $this->getRequestOptions();
        $options->addQueryParameters(array_merge(compact('index'), $parameters));

        try {
            $response = $this->client->custom(
                self::METHOD_GET,
                '/2/hits',
                $options,
                self::ANALYTICS_HOST
            );
        } catch (Throwable $e) {
            $response = [];
        }

        return new TopHitsResponse(
            (! isset($response['hits']) ? ['hits' => []] : $response)
        );
    }

    public function getTopSearches(string $index, array $parameters = []): TopSearchesResponse
    {
        $options = $this->getRequestOptions();
        $options->addQueryParameters(array_merge(compact('index'), $parameters));

        try {
            $response = $this->client->custom(
                self::METHOD_GET,
                '/2/searches',
                $options,
                self::ANALYTICS_HOST
            );
        } catch (Throwable $e) {
            $response = [];
        }

        return new TopSearchesResponse(
            (! isset($response['searches']) ? ['searches' => []] : $response)
        );
    }

    protected function getRequestOptions(array $options = []): RequestOptions
    {
        return new RequestOptions(array_merge([
            'readTimeout' => null,
            'writeTimeout' => null,
            'connectTimeout' => null,
        ], $options));
    }
}
