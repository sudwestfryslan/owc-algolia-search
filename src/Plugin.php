<?php

namespace SudwestFryslan\Algolia;

use SudwestFryslan\Updater\Updater;

class Plugin
{
    protected Container $container;

    public function setup(Container $container = null): void
    {
        $this->container = $container ?: new Container();
        // And this is were the magic happens ( ͡° ͜ʖ ͡°)
        $this->container->set(Container::class, fn($container) => $container);

        $config = require dirname(__DIR__) . '/config/container.php';
        foreach ($config as $abstract => $factory) {
            $this->container->set($abstract, $factory);
        }

        // Legacy stuff
        require $this->container->get('plugin.path') . '/lib/helpers.php';
    }

    public function boot(): void
    {
        $this->registerServiceProviders();
        // $this->registerUpdater();
        $this->loadLanguages();

        // register_activation_hook(
        //     $this->container->get('plugin.file'),
        //     [$this, 'createFakePost']
        // );
    }

    protected function registerServiceProviders(): void
    {
        $this->container->get(Settings\ServiceProvider::class)->register();
        $this->container->get(AdminServiceProvider::class)->register();
        $this->container->get(RecordServiceProvider::class)->register();
        $this->container->get(AlgoliaServiceProvider::class)->register();
        $this->container->get(FrontendServiceProvider::class)->register();
        $this->container->get(SearchPageServiceProvider::class)->register();

        $this->container->get(Post\ExcludePostServiceProvider::class)->register();

        if (apply_filters('algolia_load_extended_service', true)) {
            $this->container->get(ExtendedAlgoliaServiceProvider::class)->register();
        }
    }

    // protected function registerUpdater(): void
    // {
    //     $updater = new Updater($this->container->get('plugin.slug'));
    //     $updater->register();
    // }

    protected function loadLanguages(): void
    {
        load_plugin_textdomain(
            'owc-algolia',
            false,
            basename($this->container->get('plugin.path')) . '/languages'
        );
    }
}
