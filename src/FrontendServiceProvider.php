<?php

namespace SudwestFryslan\Algolia;

use SudwestFryslan\Algolia\Analytics\TopHitsPostResolver;

class FrontendServiceProvider extends ServiceProvider
{
    protected Settings\Manager $settings;
    protected AssetLoader $assetLoader;

    public function __construct(
        Container $container,
        Settings\Manager $manager,
        AssetLoader $loader
    ) {
        parent::__construct($container);

        $this->settings = $manager;
        $this->assetLoader = $loader;
    }

    public function register(): void
    {
        if ($this->settings->autocompleteEnabled()) {
            add_action('wp_enqueue_scripts', array($this, 'registerAutocompleteScripts'), 10);
            add_action('wp_footer', array($this, 'injectAutocompleteTemplate'), 10);
        }
    }

    public function injectAutocompleteTemplate()
    {
        ob_start();

        require $this->container->get('plugin.path') . '/views/view-owc-algolia-autocomplete-template.php';

        echo apply_filters('algolia_autocomplete_template', ob_get_clean());
    }

    public function registerAutocompleteScripts(): void
    {
        wp_enqueue_style(
            'owc_algolia',
            $this->assetLoader->getUrl('css/front.css'),
            false,
            $this->container->get('plugin.version')
        );

        wp_enqueue_script(
            'owc_algolia',
            $this->assetLoader->getUrl('js/front.js'),
            ['jquery', 'wp-util'],
            $this->container->get('plugin.version'),
            true
        );

        wp_localize_script('owc_algolia', 'algolia', [
            'index_name'        => $this->settings->getIndexName(),
            'application'       => $this->settings->getApplicationId(),
            'api_key'           => $this->settings->getSearchApiKey(),
            'placeholder'       => $this->settings->autocompletePlaceholder(),
            'query'             => get_search_query(),
            'popular_pages'     => $this->getPopularPages(),
            'search_url'        => home_url('/') . '?s=',
            'append_to'         => apply_filters('algolia_autocomplete_wrap', false),
            'append_to_mobile'  => apply_filters('algolia_autocomplete_wrap_mobile', false),
            'client_ip'         => $this->getClientIpAddress(),
            'no_insight_ips'    => $this->container->get('office.ips'),
        ]);
    }

    protected function getPopularPages()
    {
        $cache = get_transient('owc_algolia_popular_pages');

        if (! empty($cache)) {
            return $cache;
        }

        $analytics = $this->container->get(AlgoliaAnalytics::class);
        $search = $this->container->get(AlgoliaSearch::class);

        $hitsResponse = $analytics->getTopHits(
            $this->settings->getIndexName(),
            ['limit' => 4]
        );

        if ($hitsResponse->isEmpty()) {
            return [];
        }

        $topHitsPostsResolver = new TopHitsPostResolver($search->getIndex(), $hitsResponse);

        $hits = $topHitsPostsResolver->map(function ($item) {
            $item['post_excerpt'] = strip_tags($item['post_excerpt'] ?? '');

            return $item;
        })->get();

        set_transient('owc_algolia_popular_pages', $hits, DAY_IN_SECONDS);

        return $hits;
    }

    protected function getClientIpAddress(): string
    {
        $address = $_SERVER['REMOTE_ADDR'] ?? '';

        if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
            $address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        return filter_var($address, FILTER_VALIDATE_IP) ?: '';
    }
}
