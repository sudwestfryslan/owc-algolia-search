<?php

namespace SudwestFryslan\Algolia\Settings;

use SudwestFryslan\Algolia\Settings\Notices;

class MainSettings extends SettingsPage
{
    protected string $page = 'algolia-options';
    protected string $section = 'algolia-section';

    public function register(): void
    {
        $this->addSections();
        $this->addFields();
        $this->addSettings();
    }

    protected function addSections(): void
    {
        $this->section('algolia-section', __('API settings', 'owc-algolia'));
    }

    public function addFields(): void
    {
        $this->textField(
            'algolia_application_id',
            __('Application id', 'owc-algolia')
        );

        $this->textField(
            'algolia_admin_api_key',
            __('Admin API Key', 'owc-algolia')
        );

        $this->textField(
            'algolia_search_api_key',
            __('Search API Key', 'owc-algolia'),
            __('This key is used on front-end searches', 'owc-algolia')
        );

        $indexDescription = sprintf(
            __('This key is used on back-end searches, for example:', 'owc-algolia') . '<br>- %s/s=search_query<br>- %s/search/search_query)',
            home_url(),
            home_url()
        );

        $this->textField(
            'algolia_wordpress_search_api_key',
            __('WordPress search API Key', 'owc-algolia'),
            $indexDescription
        );

        $this->textField(
            'algolia_index_name',
            __('Index name', 'owc-algolia'),
            __('Enter the main index name you want to fill.', 'owc-algolia')
        );

        $indexDescription = sprintf(
            '%s<br/>%s',
            __('Optional: make sure this is a unique identifier if you are using multiple sites to fill the same index.', 'owc-algolia'),
            __('This is used to identify the right records for this site on a (re)index or on saving/deleting a single record.', 'owc-algolia')
        );

        $this->textField(
            'algolia_index_identifier',
            __('Site identifier', 'owc-algolia'),
            $indexDescription
        );

        $this->checkboxField(
            'algolia_front_end_autocomplete',
            __('Enable autocomplete', 'owc-algolia'),
            __('Enqueue the scripts to allow autocomplete', 'owc-algolia'),
            sprintf(__('Optional: make sure the element for autocomplete has the class %s', 'owc-algolia'), '<code>algolia-search</code>')
        );

        $this->textField(
            'algolia_front_end_placeholder',
            __('Autocomplete placeholder', 'owc-algolia'),
            sprintf(__('Searchbar placeholder. Only relevent if autocomplete is enabled.'), '<code>algolia-search</code>')
        );

        $this->checkboxField(
            'algolia_front_end_view_all_link',
            __('Autocomplete footer', 'owc-algolia'),
            __('Show a view all results link as footer', 'owc-algolia'),
            __('Optional: outputs a show all link after the autocomplete options', 'owc-algolia')
        );

        $this->checkboxField(
            'algolia_front_end_search',
            __('Replace default WP search', 'owc-algolia'),
            __('Return the search results from Algolia on the search page', 'owc-algolia'),
            __('Warning: when you use multiple sites to fill the same index, you need to make some template changes', 'owc-algolia')
        );
    }

    public function addSettings(): void
    {
        $this->setting('algolia_application_id');
        $this->setting('algolia_admin_api_key');
        $this->setting('algolia_search_api_key');
        $this->setting('algolia_wordpress_search_api_key');

        $this->setting('algolia_index_name');
        $this->setting('algolia_index_identifier')
            ->beforeUpdate(function ($value, $old) {
                if ($value !== $old) {
                    $value = substr(sanitize_title($value), 0, 20);
                }

                return $value;
            })->onUpdate(function ($old, $value) {
                // Check if there was a change
                if (! $old || $old === $value) {
                    return $value;
                }

                owc_algolia_queue_clean_by_index_id($old);

                $this->container->get(Notices::class)->add(sprintf(
                    '<strong>%s</strong> %s',
                    __('Warning:', 'owc-algolia'),
                    __("%s You just changed the unique identifier. The index will automatically be cleared and a reindex will be fired! If you don't want to wait, you can run a manual reindex in about 30 seconds!", 'owc-algolia')
                ));
            });

        $this->setting('algolia_front_end_autocomplete');
        $this->setting('algolia_front_end_placeholder');
        $this->setting('algolia_front_end_view_all_link');
        $this->setting('algolia_front_end_search');
    }
}
