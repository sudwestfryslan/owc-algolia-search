<?php

namespace SudwestFryslan\Algolia\Settings;

use SudwestFryslan\Algolia\Settings\Notices;

class PosttypeSettings extends SettingsPage
{
    protected string $page = 'algolia-post_types';
    protected string $section = 'algolia-post_types';

    public function register(): void
    {
        $this->setting('algolia_index_post_types')
            ->beforeUpdate([$this, 'filterIndexPostTypes'])
            ->onUpdate([$this, 'validatePosttypeChange']);
    }

    public function filterIndexPostTypes($values): array
    {
        $results = [];

        if (empty($values) || ! is_array($values)) {
            return $results;
        }

        foreach ($values as $postType => $options) {
            if (! isset($options['active']) || $options['active'] != 1) {
                continue;
            }

            unset($options['active']);
            $results[$postType] = $options;
        }

        return $results;
    }

    public function validatePosttypeChange($oldValue, $value): void
    {
        if (md5(json_encode($value)) !== md5(json_encode($oldValue))) {
            owc_algolia_queue_clean();

            $this->container->get(Notices::class)->add(sprintf(
                '<strong>%s</strong>%s',
                __('Warning:', 'owc-algolia'),
                __("You just changed the post type information. The index will automatically be cleared and a reindex will be fired! If you don't want to wait, you can run a manual reindex in about 30 seconds.", 'owc-algolia')
            ));
        }
    }
}
