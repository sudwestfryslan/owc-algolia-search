<?php

namespace SudwestFryslan\Algolia\Settings;

class Manager
{
    protected Storage\StorageDriverInterface $driver;

    public function __construct(Storage\StorageDriverInterface $driver)
    {
        $this->driver = $driver;
    }

    public function autocompleteEnabled(): bool
    {
        return $this->driver->get('algolia_front_end_autocomplete') == 1;
    }

    public function autocompletePlaceholder(): string
    {
        return (string) $this->driver->get('algolia_front_end_placeholder');
    }

    public function frontendSearchEnabled(): bool
    {
        return $this->driver->get('algolia_front_end_search') == 1;
    }

    public function getApplicationId()
    {
        $applicationId = $this->driver->get('algolia_application_id', '');

        return apply_filters('algolia_application_id', $applicationId, $this);
    }

    public function getAdminApiKey()
    {
        $adminApiKey = $this->driver->get('algolia_admin_api_key', '');

        return apply_filters('algolia_admin_api_key', $adminApiKey, $this);
    }

    public function getSearchApiKey()
    {
        $searchApiKey = $this->driver->get('algolia_search_api_key', '');

        return apply_filters('algolia_search_api_key', $searchApiKey, $this);
    }

    public function getWordPressSearchApiKey()
    {
        $searchApiKey = $this->driver->get('algolia_wordpress_search_api_key', '');

        return apply_filters('algolia_wordpress_search_api_key', $searchApiKey, $this);
    }

    public function getIndexName()
    {
        $name = $this->driver->get('algolia_index_name', '');

        return apply_filters('algolia_index_name', $name, $this);
    }

    public function getIndexIdentifier()
    {
        $index_identifier = $this->driver->get('algolia_index_identifier', '');

        return apply_filters('algolia_index_identifier', $index_identifier, $this);
    }

    public function getIndexablePostTypes(): array
    {
        $indexablePostTypes = $this->driver->get('algolia_index_post_types', []);

        return (array) apply_filters('algolia_index_post_types', $indexablePostTypes, $this);
    }

    public function getIndexablePostTypeNames(): array
    {
        $postTypes = array_keys($this->getIndexablePostTypes());

        return (array) apply_filters('algolia_post_types', $postTypes, $this);
    }

    public function getIndexablePostStatuses(): array
    {
        return (array) apply_filters('algolia_post_status', ['publish', 'inherit'], $this);
    }
}
