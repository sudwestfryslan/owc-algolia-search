<?php

namespace SudwestFryslan\Algolia\Settings;

use SudwestFryslan\Algolia\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register(): void
    {
        add_action('admin_menu', array($this, 'registerMenuPages'), 10, 2);

        add_action('admin_init', [$this->container->get(MainSettings::class), 'register']);
        add_action('admin_init', [$this->container->get(PosttypeSettings::class), 'register']);
        add_action('admin_init', [$this->container->get(AdvancedSettings::class), 'register']);
    }

    public function registerMenuPages(): void
    {
        add_menu_page(
            __('Algolia', 'owc-algolia'),
            __('Algolia', 'owc-algolia'),
            'manage_options',
            'algolia-settings',
            function () {
                require $this->container->get('plugin.path') . '/views/view-owc-algolia-admin-page.php';
            },
            'dashicons-search',
            80
        );

        if (owc_algolia_is_active()) {
            add_submenu_page(
                'algolia-settings',
                __('Algolia', 'owc-algolia'),
                __('Settings', 'owc-algolia'),
                'manage_options',
                'algolia-settings'
            );

            add_submenu_page(
                'algolia-settings',
                __('Post Types', 'owc-algolia'),
                __('Post Types', 'owc-algolia'),
                'manage_options',
                'algolia-post_types',
                array($this, 'showPostTypesPage')
            );

            add_submenu_page(
                'algolia-settings',
                __('Advanced', 'owc-algolia'),
                __('Advanced', 'owc-algolia'),
                'manage_options',
                'algolia-advanced',
                array($this, 'showAdvancedPage')
            );
        }
    }

    public function showPostTypesPage(): void
    {
        $args = apply_filters('algolia_post_type_args', ['public' => true]);

        $post_types = get_post_types($args, 'objects');

        // Get the values
        $values = get_option('algolia_index_post_types');
        $values = ($values && is_array($values) ? $values : []);

        require $this->container->get('plugin.path') . '/views/view-owc-algolia-admin-page--post_types.php';
    }

    public function showAdvancedPage(): void
    {
        require $this->container->get('plugin.path') . '/views/view-owc-algolia-admin-page--advanced.php';
    }
}
