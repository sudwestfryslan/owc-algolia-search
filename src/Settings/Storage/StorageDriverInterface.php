<?php

namespace SudwestFryslan\Algolia\Settings\Storage;

interface StorageDriverInterface
{
    public function get(string $name, $default = null);

    public function has(string $name): bool;

    public function set(string $name, $value): bool;

    public function delete(string $name): bool;
}
