<?php

namespace SudwestFryslan\Algolia\Settings\Storage;

class TransientStorageDriver implements StorageDriverInterface
{
    const EXPIRATION_FOREVER = 0;
    const EXPIRATION_MINUTE = 60;
    const EXPIRATION_HOUR = 3600;
    const EXPIRATION_DAY = 216000;
    const EXPIRATION_WEEK = 1512000;
    const EXPIRATION_MONTH = 6696000; // Assuming 31 days in a month
    const EXPIRATION_YEAR = 78840000;

    public function get(string $name, $default = null)
    {
        $value = get_transient($name);

        return $value === false ? $default : $value;
    }

    public function has(string $name): bool
    {
        return $this->get($name, null) !== null;
    }

    public function set(string $name, $value, int $expiration = self::EXPIRATION_FOREVER): bool
    {
        return (bool) set_transient($name, $value, $expiration);
    }

    public function delete(string $name): bool
    {
        return (bool) delete_transient($name);
    }
}
