<?php

namespace SudwestFryslan\Algolia\Settings\Storage;

class OptionStorageDriver implements StorageDriverInterface
{
    public function get(string $name, $default = null)
    {
        return get_option($name, $default);
    }

    public function has(string $name): bool
    {
        return $this->get($name, null) !== null;
    }

    public function set(string $name, $value): bool
    {
        return (bool) update_option($name, $value);
    }

    public function delete(string $name): bool
    {
        return (bool) delete_option($name);
    }
}
