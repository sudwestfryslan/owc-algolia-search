<?php

namespace SudwestFryslan\Algolia\Settings;

class AdvancedSettings extends SettingsPage
{
    protected string $page = 'algolia-advanced-options';
    protected string $section = 'algolia-advanced';

    public function register(): void
    {
        $this->section('algolia-advanced', '');

        $this->selectField(
            'algolia_permalink_method',
            __('Permalink method', 'owc-algolia'),
            [
                ''          => sprintf(__('Default (use %s as base)', 'owc-algolia'), \site_url()),
                'relative'  => __('Relative urls', 'owc-algolia'),
                'custom'    => __('Custom base url', 'owc-algolia')
            ],
        );

        $this->field(
            'algolia_custom_permalink',
            __('Custom permalink', 'owc-algolia'),
            fn() => print $this->htmlInput('algolia_custom_permalink', \site_url())
        );

        $this->setting('algolia_permalink_method');
        $this->setting('algolia_custom_permalink')
            ->beforeUpdate(function ($value, $old) {
                if ($value !== $old) {
                    $value = rtrim(esc_url($value), '/');
                }

                return $value;
            });
    }
}
