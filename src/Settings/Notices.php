<?php

namespace SudwestFryslan\Algolia\Settings;

use SudwestFryslan\Algolia\Settings\Storage\StorageDriverInterface;
use SudwestFryslan\Algolia\Settings\Storage\TransientStorageDriver;

class Notices
{
    protected string $storageKey = 'owc_algolia_msg';
    protected StorageDriverInterface $storage;
    protected int $storageExpiration = TransientStorageDriver::EXPIRATION_FOREVER;

    public function __construct(StorageDriverInterface $storage)
    {
        $this->storage = $storage;
    }

    public function add(string $text, string $type = 'warning'): self
    {
        $current = $this->get();
        $current[] = compact('text', 'type');

        $this->storage->set($this->storageKey, $current, $this->storageExpiration);

        return $this;
    }

    public function get(): array
    {
        return (array) $this->storage->get($this->storageKey, []);
    }

    public function flush(): bool
    {
        return $this->storage->delete($this->storageKey);
    }
}
