<?php

namespace SudwestFryslan\Algolia\Settings;

class Option
{
    protected string $name;
    protected string $group;

    public function __construct(string $name, string $group)
    {
        $this->name = $name;
        $this->group = $group;
    }

    public function sanitizeInput(callable $callback, ?int $priority = 10, ?int $args = 1): Option
    {
        add_filter(sprintf('sanitize_option_%s', $this->name), $callback, $priority, $args);

        return $this;
    }

    public function beforeUpdate(callable $callback, ?int $priority = 10, ?int $args = 2): Option
    {
        add_filter(sprintf('pre_update_option_%s', $this->name), $callback, $priority, $args);

        return $this;
    }

    public function onUpdate(callable $callback, ?int $priority = 10, ?int $args = 2): Option
    {
        add_action(sprintf('update_option_%s', $this->name), $callback, $priority, $args);

        return $this;
    }
}
