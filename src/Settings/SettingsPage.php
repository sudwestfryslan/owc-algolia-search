<?php

namespace SudwestFryslan\Algolia\Settings;

use SudwestFryslan\Algolia\Container;

abstract class SettingsPage
{
    protected string $page = '';
    protected string $section = '';
    protected Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function section(
        string $identifier,
        string $title,
        ?callable $callback = null,
        ?string $page = null
    ): void {
        add_settings_section($identifier, $title, $callback, $page ?: $this->page);
    }

    public function setting(string $name, ?string $group = null): Option
    {
        register_setting($group ?: $this->section, $name);

        return new Option($name, $group ?: $this->section);
    }

    public function field(
        string $identifier,
        string $title,
        ?callable $callback,
        ?string $page = null,
        ?string $section = null,
        ?array $arguments = array()
    ): void {
        if (is_null($callback)) {
            $callbackName = sprintf('show_%s', $this->identifier);
            $callback = [$this, $callbackName];
        }

        add_settings_field(
            $identifier,
            $title,
            $callback,
            $page ?: $this->page,
            $section ?: $this->section,
            $arguments
        );
    }

    public function textField(
        string $identifier,
        string $title,
        ?string $description = ''
    ): void {
        $this->field($identifier, $title, function () use ($identifier, $description) {
            print $this->htmlInput($identifier);

            printf('<p class="description">%s</p>', $description);
        });
    }

    public function checkboxField(
        string $identifier,
        string $title,
        string $label,
        ?string $description = ''
    ): void {
        $this->field($identifier, $title, function () use ($identifier, $label, $description) {
            // Make sure an unchecked checkbox will be properly stored
            add_filter(sprintf('sanitize_option_%s', $identifier), fn($input) => (int) $input);

            print $this->htmlCheckbox($identifier, '', $label);

            printf('<p class="description">%s</p>', $description);
        });
    }

    public function selectField(
        string $identifier,
        string $title,
        array $options,
        ?string $description = ''
    ): void {
        $this->field($identifier, $title, function () use ($identifier, $options, $description) {
            print $this->htmlSelect($identifier, $options);

            printf('<p class="description">%s</p>', $description);
        });
    }

    protected function htmlInput(string $name, string $value = ''): string
    {
        $value = $value ? $value : (string) get_option($name, false);

        return sprintf(
            '<input type="text" name="%1$s" class="regular-text" id="%1$s" value="%2$s"/>',
            $name,
            $value
        );
    }

    protected function htmlCheckbox(string $name, string $value = '', string $label = ''): string
    {
        $value = $value ? $value : (string) get_option($name, false);

        return sprintf(
            '<fieldset>
                <label>
                    <input type="checkbox" name="%1$s" class="regular-text" id="%1$s" value="1" %2$s/>
                    %3$s
                </label>
            </fieldset>',
            $name,
            \checked($value, 1, false) ? 'checked' : '',
            $label ?: $name
        );
    }

    protected function htmlSelect(string $name, array $options, string $value = ''): string
    {
        $value = $value ? $value : (string) get_option($name, false);

        $htmlOptions = $this->htmlSelectOptions($options, $value);

        return sprintf(
            '<select name="%1$s" class="regular-text" id="%1$s">%2$s</select>',
            $name,
            $htmlOptions
        );
    }

    protected function htmlSelectOptions(array $options, string $value): string
    {
        $htmlOptions = '';

        foreach ($options as $option => $label) {
            $htmlOptions .= sprintf(
                '<option value="%1$s" %2$s>%3$s</option>',
                $option,
                \selected($option, $value, false) ? 'selected' : '',
                $label
            );
        }

        return $htmlOptions;
    }
}
