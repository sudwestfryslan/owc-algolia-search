<?php

declare(strict_types=1);

namespace SudwestFryslan\Algolia\Collection;

use Closure;

class Collection extends Enumerable
{
    public function collect(array $data): Collection
    {
        return new self($data);
    }

    public function get($key, $default = null)
    {
        return $this->has($key) ? $this->data[$key] : $default;
    }

    public function has($key)
    {
        return isset($this->data[$key]);
    }

    public function all(): array
    {
        return $this->data;
    }

    public function push($item): Collection
    {
        $this->data[] = $item;

        return $this;
    }

    public function count()
    {
        return count($this->data);
    }

    public function isEmpty(): bool
    {
        return empty($this->data);
    }

    public function isNotEmpty(): bool
    {
        return $this->isEmpty() === false;
    }

    public function nth(int $index, $default)
    {
        return $this->get($index, $default);
    }

    public function first()
    {
        return reset($this->data);
    }

    public function last()
    {
        return end($this->data);
    }

    public function filter(Closure $predicate): Collection
    {
        return $this->collect(array_filter($this->data, $predicate));
    }

    public function map(Closure $callback): Collection
    {
        return $this->collect(array_map($callback, $this->data));
    }
}
