<?php

namespace SudwestFryslan\Algolia\Analytics;

use InvalidArgumentException;

class TopSearchesResponse
{
    protected array $response;
    protected array $searches;

    public function __construct(array $apiResponse)
    {
        $this->response = $apiResponse;
        $this->searches = $this->parseApiResponse($apiResponse);
    }

    public function isEmpty()
    {
        return empty($this->searches);
    }

    public function words()
    {
        foreach ($this->all() as $search) {
            yield $search['search'] ?? '';
        }
    }

    public function all(): array
    {
        return $this->searches;
    }

    protected function parseApiResponse($response)
    {
        if (!isset($response['searches'])) {
            throw new InvalidArgumentException("The given response is not properly formatted.");
        }

        return empty($response['searches']) ? [] : $response['searches'];
    }
}