<?php

namespace SudwestFryslan\Algolia\Analytics;

use Closure;
use Algolia\AlgoliaSearch\SearchIndex;

class TopHitsPostResolver
{
    protected TopHitsResponse $response;
    protected SearchIndex $client;
    protected ?Closure $processor;

    public function __construct(SearchIndex $indexClient, TopHitsResponse $response)
    {
        $this->client = $indexClient;
        $this->response = $response;
    }

    public function map(Closure $processor)
    {
        $this->processor = $processor;

        return $this;
    }

    public function getProcessor(): Closure
    {
        return $this->processor ?: fn($item) => $item;
    }

    public function get(): array
    {
        $objectIds = array_map(function($item) {
            return $item['hit'] ?? '';
        }, $this->response->all());

        $results = $this->client->getObjects(array_filter($objectIds));
        
        return array_map($this->getProcessor(), ($results['results'] ?? []));
    }
}