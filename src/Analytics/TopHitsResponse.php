<?php

namespace SudwestFryslan\Algolia\Analytics;

use InvalidArgumentException;

class TopHitsResponse
{
    protected array $response;
    protected array $hits;

    public function __construct(array $apiResponse)
    {
        $this->response = $apiResponse;
        $this->hits = $this->parseApiResponse($apiResponse);
    }

    public function isEmpty(): bool
    {
        return empty($this->hits);
    }

    public function identifiers()
    {
        foreach ($this->hits as $hit) {
            yield substr($hit['hit'], (strrpos($hit['hit'], '#') + 1));
        }
    }

    public function all(): array
    {
        return $this->hits;
    }

    protected function parseApiResponse($response): array
    {
        if (!isset($response['hits'])) {
            throw new InvalidArgumentException("The given response is not properly formatted.");
        }

        return empty($response['hits']) ? [] : $response['hits'];
    }
}