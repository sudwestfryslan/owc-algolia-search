<?php

namespace SudwestFryslan\Algolia\Post\Keywords;

use SudwestFryslan\Algolia\Post;

class Resolver
{
    protected array $strategies;

    public function __construct(array $strategies)
    {
        foreach ($strategies as $strategy) {
            $this->setStrategy($strategy);
        }
    }

    public function setStrategy(KeywordsStrategy $strategy): Resolver
    {
        $this->strategies[] = $strategy;

        return $this;
    }

    public function execute(Post $post): array
    {
        $keywords = [];

        foreach ($this->strategies as $strategy) {
            $keywords = array_merge($keywords, $strategy->get($post));
        }

        return $keywords;
    }
}
