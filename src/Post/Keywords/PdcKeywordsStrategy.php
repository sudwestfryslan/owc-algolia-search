<?php

namespace SudwestFryslan\Algolia\Post\Keywords;

use SudwestFryslan\Algolia\Post;

class PdcKeywordsStrategy extends KeywordsStrategy
{
    public function get(Post $post): array
    {
        $tags = $this->getRawTags($post);

        $delimiter = (strpos($tags, ';') !== false) ? ';' : ',';

        $items = explode($delimiter, (string) $tags);

        return empty($items) ? [] : $items;
    }

    protected function getRawTags(Post $post): string
    {
        return (string) get_post_meta($post->ID, '_owc_pdc_tags', true);
    }
}
