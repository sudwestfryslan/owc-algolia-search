<?php

namespace SudwestFryslan\Algolia\Post\Keywords;

use SudwestFryslan\Algolia\Post;

abstract class KeywordsStrategy
{
    abstract public function get(Post $post): array;
}
