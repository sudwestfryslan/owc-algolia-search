<?php

namespace SudwestFryslan\Algolia\Post\Keywords;

use SudwestFryslan\Algolia\Post;

class YoastKeywordsStrategy extends KeywordsStrategy
{
    public function get(Post $post): array
    {
        $items = $this->getKeywordsArray($post);

        $items = array_reduce($items, function ($carry, $item) {
            $delimiter = (strpos($item, ';') !== false) ? ';' : ',';

            $split = explode($delimiter, (string) $item);

            return array_merge($carry, $split);
        }, []);

        return $items;
    }

    protected function getKeywordsArray($post): array
    {
        $raw = (string) get_post_meta($post->ID, '_yoast_wpseo_keywordsynonyms', true);

        if (empty($raw)) {
            return [];
        }

        $items = json_decode((string) $raw, true);

        return json_last_error() === JSON_ERROR_NONE ? $items : [];
    }
}
