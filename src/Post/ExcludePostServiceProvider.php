<?php

namespace SudwestFryslan\Algolia\Post;

use WP_Post;
use SudwestFryslan\Algolia\Post;
use SudwestFryslan\Algolia\Container;
use SudwestFryslan\Algolia\AlgoliaServiceProvider;
use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;
use SudwestFryslan\Algolia\ServiceProvider as BaseServiceProvider;

class ExcludePostServiceProvider extends BaseServiceProvider
{
    protected SettingsManager $settings;

    public function __construct(Container $container, SettingsManager $manager)
    {
        parent::__construct($container);

        $this->settings = $manager;
    }

    public function register(): void
    {
        add_action('add_meta_boxes', [$this, 'setupMetabox']);
        add_filter('algolia_post_allowed', [$this, 'preventExcludedPosts'], 10, 2);
        add_action('save_post', [$this, 'saveExcludeOption'], 10, 2);
    }

    public function preventExcludedPosts(bool $allowed, WP_Post $post): bool
    {
        $algoliaPost = new Post($post);

        // Only overwrite the allowed variable if it is currently allowed.
        // Otherwise we might be reversing other plugin logic where
        // a post for whatever reason needs to be disallowed.
        if ($allowed) {
            $allowed = ! $algoliaPost->isExcluded();
        }

        return $allowed;
    }

    public function setupMetabox($postType)
    {
        $postTypes = $this->settings->getIndexablePostTypeNames();

        if (! in_array($postType, $postTypes)) {
            return;
        }

        add_meta_box(
            'algolia_search_box',
            'Zoekmachine (Algolia)',
            function () {
                global $post;

                $algoliaPost = new Post($post);

                require $this->container->get('plugin.path') . '/views/admin.metabox.php';
            },
            $postType,
            'side'
        );
    }

    public function saveExcludeOption(int $postId, WP_Post $post)
    {
        if (\wp_is_post_revision($postId) || \wp_is_post_autosave($postId)) {
            return;
        }

        $excluded = (bool) ($_POST['_algolia_exclude'] ?? false);
        $algoliaPost = new Post($post);
        $algoliaPost->setExcluded($excluded);

        if ($excluded) {
            $this->container->get(AlgoliaServiceProvider::class)->removePostFromIndex($post);
        }

        return true;
    }
}
