<?php

namespace SudwestFryslan\Algolia;

use Algolia\AlgoliaSearch\SearchIndex;
use Algolia\AlgoliaSearch\SearchClient;
use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;

class AlgoliaSearch
{
    protected SearchClient $client;
    protected SettingsManager $settings;

    public function __construct(SearchClient $client, SettingsManager $settings)
    {
        $this->client = $client;
        $this->settings = $settings;
    }

    public function getIndex(): SearchIndex
    {
        return $this->client->initIndex(
            apply_filters(
                'algolia_main_index_name',
                $this->settings->getIndexName()
            )
        );
    }
}
