<?php

namespace SudwestFryslan\Algolia;

use Throwable;
use SudwestFryslan\Algolia\Post;

class ExtendedAlgoliaServiceProvider extends ServiceProvider
{
    protected $allowed_mime_types = ['application/pdf'];

    public function register(): void
    {
         // Add the post record filter
        // add_filter('algolia_post_to_record', array($this, 'set_record_fields'), 10, 3);

        // Filter none document attachments
        add_filter('algolia_post_allowed', array($this, 'filter_attachment_types'), 10, 2);
    }

    // public function sanitize_pdf_text($text)
    // {
    //     // Remove tabs
    //     $text = trim(preg_replace('/\t+/', '', $text));

    //     // Remove double spaces and line breaks
    //     $text = preg_replace('/\s+/', ' ', $text);

    //     // Sanitize the text
    //     return sanitize_text_field($text);
    // }

    // public function get_pdf_text($post_id)
    // {
    //     $attachment = get_attached_file($post_id);
    //     $text       = false;

    //     if ($attachment && $attachment !== '' && file_exists($attachment)) {
    //         $parser   = new \Smalot\PdfParser\Parser();
    //         $pdf      = $parser->parseFile($attachment);

    //         // Check if pdf is not empty
    //         if ($pdf != "") {
    //             try {
    //                 // Set the pdf text
    //                 $text = $pdf->getText();
    //                 $text = $this->sanitize_pdf_text($text);
    //             } catch (Throwable $e) {
    //             }
    //         }
    //     }

    //     return $text;
    // }

    /**
     * @param int $post_id
     *
     * @return array
     */
    // public function get_post_images($post)
    // {
    //     $images = array();

    //     if ($post->post_type === 'attachment') {
    //         $post_thumbnail_id = (int) $post->ID;
    //     } else {
    //         $post_thumbnail_id = get_post_thumbnail_id($post->ID);
    //     }

    //     if ($post_thumbnail_id) {
    //         $sizes = (array) apply_filters('algolia_post_images_sizes', array( 'thumbnail' ));
    //         foreach ($sizes as $size) {
    //             $info = wp_get_attachment_image_src($post_thumbnail_id, $size);
    //             if (! $info) {
    //                 continue;
    //             }

    //             $images[ $size ] = array(
    //                 'url'    => $info[0],
    //                 'width'  => $info[1],
    //                 'height' => $info[2],
    //             );
    //         }
    //     }

    //     return (array) apply_filters('algolia_get_post_images', $images);
    // }

    // public function get_taxonomies($post)
    // {
    //     $results = array();

    //     // Get the taxonomy objects
    //     $taxonomy_objects = get_object_taxonomies($post->post_type, 'objects');

    //     foreach ($taxonomy_objects as $taxonomy) {
    //         $terms = wp_get_object_terms($post->ID, $taxonomy->name);
    //         $terms = is_array($terms) ? $terms : array();

    //         $taxonomy_values = wp_list_pluck($terms, 'name');
    //         if (! empty($taxonomy_values)) {
    //             $results[ $taxonomy->name ] = $taxonomy_values;
    //         }
    //     }

    //     return $results;
    // }

    // public function get_post_content($post)
    // {
    //     $content = apply_filters('the_content', $post->post_content);

    //     // Strip all html tags
    //     $content = wp_strip_all_tags($content, true);

    //     // Decode the html characters
    //     return html_entity_decode($content);
    // }

    // public function get_post_record($post, $objectID)
    // {
    //     $record = array(
    //         'post_id'               => $post->ID,
    //         'post_type'             => $post->post_type,
    //         'post_title'            => $post->post_title,
    //         'post_content'          => $this->get_post_content($post),
    //         'post_excerpt'          => apply_filters('the_excerpt', $post->post_excerpt),
    //         'post_date'             => get_post_time('U', false, $post),
    //         'post_date_formatted'   => get_the_date('', $post),
    //         'post_modified'         => get_post_modified_time('U', false, $post),
    //         'comment_count'         => (int) $post->comment_count,
    //         'menu_order'            => (int) $post->menu_order,
    //         'taxonomies'            => $this->get_taxonomies($post),
    //         'images'                => $this->get_post_images($post),
    //         'permalink'             => owc_algolia_permalink($post),
    //         'is_sticky'             => (is_sticky($post->ID) ? 1 : 0),
    //         'objectID'              => $objectID
    //     );

    //     // Return the record
    //     return apply_filters('algolia_post_to_record_filtered', $record, $post);
    // }

    // public function get_attachment_record($post, $objectID)
    // {
    //     $record = $this->get_post_record($post, $objectID);

    //     // Get the mime type
    //     $mime_type  = get_post_mime_type($post);

    //     // Add the attachment information
    //     $record['meta']['mime_type'] = $mime_type;
    //     $record['meta']['alt'] = get_post_meta($post->ID, '_wp_attachment_image_alt', true);

    //     // Check if it is a pdf file
    //     if ($mime_type == 'application/pdf') {
    //         $pdf_text = false;

    //         try {
    //             // Get the pdf text
    //             $pdf_text = $this->get_pdf_text($post->ID);
    //         } catch (Throwable $e) {
    //             // Something went wrong with the text extraction
    //         }

    //         // If pdf text is received
    //         if ($pdf_text && !empty($pdf_text)) {
    //             // Set the post content
    //             $record['post_content'] = $pdf_text;
    //         } else {
    //             // Empty the post content
    //             $record['post_content'] = '';
    //         }
    //     }

    //      // Return the record
    //     return apply_filters('algolia_attachment_to_record_filtered', $record, $post);
    // }

    // public function set_record_fields(array $data, Post $post, $objectID)
    // {
    //     if ($post->post_type == 'attachment') {
    //         return $this->get_attachment_record($post, $objectID);
    //     }

    //     // return array_merge($data, $this->get_post_record($post, $objectID));
    // }

    public function filter_attachment_types($allowed, $post)
    {

        // Check if it is an attachment post type
        if ($post->post_type == 'attachment') {
            // Get the mime type
            $mime_type = get_post_mime_type($post);

            // Check if it is of the correct mime type
            if (!in_array($mime_type, $this->allowed_mime_types)) {
                return false;
            }
        }


        return $allowed;
    }
}
