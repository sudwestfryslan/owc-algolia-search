<?php

namespace SudwestFryslan\Algolia;

use WP_CLI;
use WP_Post;

class AlgoliaServiceProvider extends ServiceProvider
{
    protected int $postsPerIndex = 20;
    protected array $post_status = ['publish', 'inherit'];
    protected bool $splitRecords = true;
    protected int $maxRecordLength = 7000;

    protected Settings\Manager $settings;

    public function __construct(Container $container, Settings\Manager $manager)
    {
        parent::__construct($container);

        $this->settings = $manager;
    }

    public function register(): void
    {
        // Handle post mutations (queue based)
        add_action('save_post', [$this, 'algolia_queue_update_post'], 99, 2);

        // Handle postmeta mutations (queue based)
        add_action('added_post_meta', [$this, 'algolia_queue_update_post_meta'], 99, 4);
        add_action('updated_post_meta', [$this, 'algolia_queue_update_post_meta'], 99, 4);
        add_action('deleted_post_meta', [$this, 'algolia_queue_update_post_meta'], 99, 4);

        // Handle attachment mutations
        add_action('add_attachment', [$this, 'algolia_update_attachment'], 99, 1);
        add_action('attachment_updated', [$this, 'algolia_update_attachment'], 99, 1);

        // Delete the post from algolia if exists
        add_action('delete_post', [$this, 'algolia_delete_post'], 99, 1);
        add_action('transition_post_status', function ($newStatus, $oldStatus, $post) {
            if ($newStatus !== 'publish') {
                return $this->removePostFromIndex($post);
            }
        }, 10, 3);

        // Run the action to update the post from queue
        add_action('algolia_update_post_from_queue', [$this, 'update_posts_from_queue'], 10, 1);

        // Run the action to cleanup, reindex
        add_action('algolia_clean_by_index_id_from_queue', [$this, 'clean_by_index_id_from_queue'], 10, 1);
        // add_action('algolia_clean_from_queue', [$this, 'clear_index'], 10);

        // add_action('algolia_start_reindex_from_queue', [$this, 'start_reindex_from_queue'], 10);
        // add_action('algolia_reindex_from_queue', [$this, 'reindex_from_queue'], 10);

        // Add ajax index actions
        add_action('wp_ajax_algolia_reindex_start', [$this, 'ajax_start_reindex_posts']);
        add_action('wp_ajax_algolia_reindex', [$this, 'ajax_reindex_posts']);

        if (\defined('WP_CLI') && WP_CLI) {
            $reindexCommand = new Commands\Reindex(
                $this->container->get(Settings\Manager::class),
                $this->container->get(AlgoliaSearch::class)
            );

            $reindexCommand->setRecordSplitter(function ($post) {
                return $this->get_split_record($post->toWP_PostInstance());
            });

            WP_CLI::add_command('algolia reindex', $reindexCommand);
        }
    }

    public function removePostFromIndex(WP_Post $post)
    {
        $index = $this->container->get(AlgoliaSearch::class)->getIndex();
        $objectIdentifier = $this->get_object_id($post);

        return $index->deleteObject($objectIdentifier);
    }

    public function get_post_type_priority($post_type, $default = 10)
    {
        $post_types = $this->settings->getIndexablePostTypes();
        $priority = $post_types[$post_type]['prio'] ?? $default;

        return \intval($priority);
    }

    public function get_reindex()
    {
        return get_option('algolia_reindex_queue', false);
    }

    public function set_reindex($post_ids = [])
    {
        return update_option('algolia_reindex_queue', $post_ids);
    }

    public function has_reindex()
    {
        $reindex = $this->container->get('setting.reindex');

        return $reindex && !empty($reindex);
    }

    public function set_split_records(bool $enabled = true)
    {
        $this->splitRecords = ($enabled === true);
    }

    public function set_split_size(int $split_size)
    {
        $this->maxRecordLength = \intval($split_size);
    }

    public function getMaxRecordLength(): int
    {
        return $this->maxRecordLength;
    }

    public function get_post_per_index()
    {
        return apply_filters('algolia_posts_per_index', $this->postsPerIndex);
    }

    public function get_posts_by_ids($postIds)
    {
        return get_posts([
            'post__in'          => $postIds,
            'post_type'         => 'any',
            'post_status'       => 'any',
            'posts_per_page'    => -1
        ]);
    }

    public function clear_index($index = false, $force = false)
    {
        $index = $index ?: $this->container->get(AlgoliaSearch::class)->getIndex();
        $index_id = $this->settings->getIndexIdentifier();

        if (!$force && $index_id && !empty($index_id)) {
            return $index->deleteBy(['filters' => 'indexID:' . $index_id])->wait();
        }

        return $index->clearObjects()->wait();
    }

    public function setup_index($index = false)
    {
        $index = ($index ? $index : $this->container->get(AlgoliaSearch::class)->getIndex());

        $settings = apply_filters('algolia_default_index_settings', [
            'attributeForDistinct'      => 'distinctID',
            'distinct'                  => true,
            'attributesForFaceting'     => [
                'filterOnly(indexID)',
                'filterOnly(distinctID)'
            ]
        ]);

        if ($settings && !empty($settings)) {
            $index->setSettings($settings);
        }
    }

    public function get_object_id(WP_Post $post, $append = false, $filter = true)
    {
        $parts = [];

        $index_identifier = $this->settings->getIndexIdentifier();

        // Check if a index identifier isset
        if ($index_identifier && !empty($index_identifier)) {
            $parts['index_id'] = $index_identifier;
        }

        // Set parts array
        $parts['post_type'] = $post->post_type;
        $parts['post_id']   = $post->ID;

        if ($append !== false) {
            $parts['append'] = $append;
        }

        // Generate the object id
        $object_id = \implode('#', $parts);

        // Return the object id
        return $filter ? apply_filters('algolia_object_id', $object_id, $post) : $object_id;
    }

    public function get_distinct_identifier($post)
    {
        // Return the object id without the append and without the filters applied
        return $this->get_object_id($post, false, false);
    }

    public function get_record(WP_Post $post)
    {
        $objectID = $this->get_object_id($post);
        $record = (array) apply_filters(
            'algolia_post_to_record',
            (array) $post,
            Post::fromWP_PostInstance($post),
            $objectID
        );

        $base = [
            'priority'      => $this->get_post_type_priority($post->post_type),
            'objectID'      => $objectID,
            'indexID'       => $this->settings->getIndexIdentifier(),
            'distinctID'    => $this->get_distinct_identifier($post),
        ];

        return \array_merge($base, $record);
    }

    public function get_split_record(WP_Post $post)
    {
        $record = $this->get_record($post);
        $recordLength = $this->getRecordLength($record);

        // If the record length does not exeed the max content size, we can
        // safely return it without splitting the record into pieces.
        if ($recordLength <= $this->getMaxRecordLength()) {
            delete_post_meta($post->ID, 'algolia_split_record');

            return [$record];
        }

        // Get the length with which we need to split the 'post_content' entry.
        $contentSplitLength = $this->getMaxRecordLength() - ($recordLength - \mb_strlen($record['post_content']));

        $splitted = [];
        $lines = \explode("\n", \wordwrap($record['post_content'], $contentSplitLength));

        foreach ($lines as $key => $line) {
            /**
             * Text from PDFs sometimes contain weird Unicode characters that
             * get expanded when encoded to json. In most cases the text
             * itself is wrangled, so we can shrink the string to the
             * max record length we've configured.
             */
            $jsLine = \json_encode($line);
            if (\mb_strlen($jsLine) > $this->maxRecordLength) {
                $line = \json_decode(\substr($jsLine, 0, $this->maxRecordLength) . '"');
            }

            $record['post_content'] = $line;
            $record['objectID'] = $this->get_object_id($post, $key);

            $splitted[] = $record;
        }

        update_post_meta($post->ID, 'algolia_split_record', 1);

        return $splitted;
    }

    public function save_record(WP_Post $post, $index)
    {
        if ($this->splitRecords) {
            $splitRecords  = $this->get_split_record($post);

            if (\count($splitRecords) > 1) {
                $distinct_id = $this->get_distinct_identifier($post);

                // Delete the split records before creating new split records
                $index->deleteBy(['filters' => 'distinctID:' . $distinct_id]);

                // Save the records
                return $index->saveObjects($splitRecords);
            }
        }

        $record = $this->get_record($post);

        return $index->saveObject($record);
    }

    public function indexing_post_allowed(WP_Post $post, $ignore_status = true)
    {
        $post_types = $this->settings->getIndexablePostTypeNames();

        if (\in_array($post->post_type, $post_types)) {
            // Set the allowed post statuses
            $post_status = $this->settings->getIndexablePostStatuses();


            if ($ignore_status || $post_status == 'any' || \in_array($post->post_status, $post_status)) {
                // Check if post is allowed to be indexed
                return apply_filters('algolia_post_allowed', true, $post, $post->post_type);
            }
        }

        // Default return false
        return false;
    }

    public function add_post_update_to_queue($post_id)
    {
        // Remove previous schedules
        wp_clear_scheduled_hook('algolia_update_post_from_queue', [$post_id]);

        // Add new single event
        wp_schedule_single_event(\time() + 30, 'algolia_update_post_from_queue', [$post_id]);
    }

    public function start_reindex_posts()
    {

        if (!$this->has_reindex()) {
            // Get the algolia index
            $index = $this->container->get(AlgoliaSearch::class)->getIndex();

            // Stop the automatic indexing operations
            owc_algolia_queue_stop();

            // Clear the index
            $this->clear_index($index);

            $post_types = $this->settings->getIndexablePostTypeNames();
            $post_status = $this->settings->getIndexablePostStatuses();

            // Get the post ids
            $post_ids   = get_posts([
                'fields'            => 'ids',
                'post_type'         => $post_types,
                'post_status'       => $post_status,
                'posts_per_page'    => -1
            ]);

            // Filter the post ids
            $post_ids = apply_filters('algolia_reindex_post_ids', $post_ids, $post_types);

            // Save the reindex queue
            $this->set_reindex($post_ids);
        }

        // Return the reindex value
        return \count($this->get_reindex());
    }

    public function reindex_posts()
    {

        // Set default reindex
        $reindex = [];

        // Check if a reindex is available
        if ($this->has_reindex()) {
            // Get the reindex queue
            $reindex    = $this->get_reindex();
            $limit      = $this->get_post_per_index();

            // Get the post ids
            $post_ids   = \array_splice($reindex, 0, $limit);

            // Check once more if the post ids is not empty
            if (!empty($post_ids)) {
                // Set empty array and loop count
                $records    = [];
                $loop_i     = 0;

                // Get the posts
                $posts = $this->get_posts_by_ids($post_ids);

                // Loop trough the posts
                foreach ($posts as $post) {
                    // Check if post is allowed to be indexed
                    $allowed = apply_filters('algolia_post_allowed', true, $post, $post->post_type);

                    // If allowed than add the record
                    if ($allowed) {
                        if ($this->splitRecords) {
                            // Get split records
                            $splitRecords  = $this->get_split_record($post);

                            // Merge the split record with records
                            $records        = \array_merge($records, $splitRecords);
                        } else {
                            // Filter the results
                            $records[] = $this->get_record($post);
                        }

                        // Add 1 to the loop count
                        $loop_i++;
                    }
                }

                if (!empty($records)) {
                    // Get the algolia index
                    $index = $this->container->get(AlgoliaSearch::class)->getIndex();

                    // Save the records
                    $index->saveObjects($records);
                }

                // Update the queue
                $results = $this->set_reindex($reindex);
            }
        }

        // Setup the index at the end of the indexing job
        if (\count($reindex) <= 0) {
            $this->setup_index();
        }

        // Return the queue
        return $reindex;
    }

    public function algolia_update_post($id, WP_Post $post)
    {

        // Check if it is autosaved or set as a revision
        if (wp_is_post_revision($id) || wp_is_post_autosave($id)) {
            return $post;
        }

        if ($this->indexing_post_allowed($post, true)) {
            // Get the Algolia index
            $index = $this->container->get(AlgoliaSearch::class)->getIndex();

            // Set the allowed post status
            $post_status = apply_filters('algolia_post_status', $this->post_status);

            // Check if post status is allowed else delete the post from Algolia
            if (\in_array($post->post_status, $post_status)) {
                // Save the record
                $this->save_record($post, $index);
            } else {
                // Get the distinct id
                $distinct_id = $this->get_distinct_identifier($post);

                // Delete the records
                $index->deleteBy(['filters' => 'distinctID:' . $distinct_id]);
            }
        }

        // Return the post object
        return $post;
    }

    public function algolia_queue_update_post($id, WP_Post $post)
    {
        // Check if the post is allowed to be indexed
        if ($this->indexing_post_allowed($post, true)) {
            // Add the post update to the queue
            $this->add_post_update_to_queue($id);
        }
    }

    public function algolia_queue_update_post_meta($meta_ids, $object_id, $meta_key, $_meta_value)
    {

        // Default set the update Algolia to false
        $update_algolia = false;

        // Check if the feature image is deleted
        if ($meta_key == '_thumbnail_id') {
            // Set the variable to update the post
            $update_algolia = true;
        }

        // Check if the post needs to be updated in Algolia
        if ($update_algolia) {
            // Get the post
            $post = get_post($object_id);

            // Check if a post object is received and the post is allowed to be indexed
            if ($post && $this->indexing_post_allowed($post, true)) {
                $this->add_post_update_to_queue($object_id);
            }
        }
    }

    public function algolia_update_attachment($post_id)
    {

        // Get the post
        $post = get_post($post_id);

        // Check if the post is allowed to be indexed
        if ($this->indexing_post_allowed($post, true)) {
            // Add the post update to the queue
            $this->add_post_update_to_queue($post_id);
        }
    }

    public function algolia_delete_post($post_id, $forced = false)
    {

        // Check if it is autosaved or set as a revision
        if (!wp_is_post_revision($post_id) && !wp_is_post_autosave($post_id)) {
            // Get the post
            $post = get_post($post_id);

            // Set the index var
            $index = $this->container->get(AlgoliaSearch::class)->getIndex();

            // Check if allowed
            if ($index && (($post && $this->indexing_post_allowed($post, true)) || $forced)) {
                // Get the distinct id
                $distinct_id = $this->get_distinct_identifier($post);

                // Delete the records
                $index->deleteBy(['filters' => 'distinctID:' . $distinct_id]);
            }
        }
    }

    public function update_posts_from_queue($post_ids)
    {

        // Make sure it is an array of post ids
        $post_ids = (!\is_array($post_ids) ? [$post_ids] : $post_ids);

        // Loop trough the post ids
        foreach ($post_ids as $post_id) {
            // Sanitize the post id
            $post_id    = \intval($post_id);

            // Get the post
            $post       = get_post($post_id);

            // Check if post isset and update the algolia post if needed
            if ($post) {
                $this->algolia_update_post($post_id, $post);
            }
        }
    }

    public function clean_by_index_id_from_queue($index_ids)
    {

        // Make sure it is an array of post ids
        $index_ids  = (!\is_array($index_ids) ? [$index_ids] : $index_ids);

        // Set the index var
        $index      = $this->container->get(AlgoliaSearch::class)->getIndex();

        // Loop trough the index ids
        foreach ($index_ids as $index_id) {
            // Sanitize the index id
            $index_id = sanitize_text_field($index_id);

            // Clear the index by index id
            $index->deleteBy(['filters' => 'indexID:' . $index_id])->wait();
        }
    }

    public function start_reindex_from_queue()
    {

        if (!owc_algolia_has_queue_clean()) {
            // Start the reindex
            $post_i = $this->start_reindex_posts();

            // Check if there are posts left to index
            if ($post_i && $post_i > 0) {
                // Schedule a reindex job in 10 seconds
                owc_algolia_queue_reindex(10);
            }
        } else {
            // Restart the event in 5 minutes
            owc_algolia_queue_start_reindex(300);
        }
    }

    public function reindex_from_queue()
    {

        if (!owc_algolia_has_queue_clean()) {
            // Reindex the posts
            $items_left     = $this->reindex_posts();
            $items_left_i   = \count($items_left);

            // Check if there are posts left to reindex
            if ($items_left_i > 0) {
                // Schedule a reindex job in 20 seconds
                owc_algolia_queue_reindex(20);
            }
        }
    }

    public function ajax_start_reindex_posts()
    {

        if (!owc_algolia_has_queue_clean()) {
            // Start the reindex
            $post_i = $this->start_reindex_posts();

            if ($post_i && $post_i > 0) {
                // Set the results array
                $results = [
                    'status'    => 'success',
                    'total'     => $post_i
                ];
            } else {
                // Set the results array
                $results = [
                    'status'    => 'error',
                    'message'   => __('Something went wrong', 'owc-algolia')
                ];
            }
        } else {
            // Set the results array
            $results = [
                'status'    => 'error',
                'message'   => __('Wait until the cleanup is finished before running a manual reindex', 'owc-algolia')
            ];
        }

        echo \json_encode($results);

        exit;
    }

    public function ajax_reindex_posts()
    {

        if (!owc_algolia_has_queue_clean()) {
            // Reindex the posts
            $items_left     = $this->reindex_posts();
            $items_left_i   = \count($items_left);

            // Set the results array
            $results = [
                'status'        => 'success',
                'items_left'    => $items_left_i
            ];
        } else {
            $results = [
                'status'    => 'error',
                'message'   => __('Wait until the cleanup is finished before running a reindex', 'owc-algolia')
            ];
        }

        echo \json_encode($results);

        exit;
    }

    /**
     * Return the the total amount of characters within the given record.
     * @param  array $record
     * @return int
     */
    protected function getRecordLength(array $record): int
    {
        $reduceCallback = function ($carry, $item) use (&$reduceCallback) {
            if (\is_array($item)) {
                return \array_reduce($item, $reduceCallback, $carry);
            }

            return $carry + \mb_strlen($item);
        };

        return (int) \array_reduce($record, $reduceCallback, 0);
    }
}
