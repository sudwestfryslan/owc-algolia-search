<?php

namespace SudwestFryslan\Algolia;

use SudwestFryslan\Algolia\Settings\Manager as SettingsManager;

class IndexablePostsQuery
{
    protected SettingsManager $settings;
    protected array $types = [];
    protected array $statuses = [];

    public function __construct(SettingsManager $settings)
    {
        $this->settings = $settings;
    }

    public function execute(): array
    {
        $types = $this->settings->getIndexablePostTypeNames();
        $statuses = $this->settings->getIndexablePostStatuses();

        if (! empty($this->types)) {
            $types = \array_intersect($types, $this->types);
        }

        if (! empty($this->statuses)) {
            $statuses = \array_intersect($statuses, $this->statuses);
        }

        $posts = get_posts([
            'post_type'         => $types,
            'post_status'       => $statuses,
            'posts_per_page'    => -1
        ]);

        return \array_map(function ($item) {
            return new Post($item);
        }, $posts);
    }

    public function ofType(string $postType)
    {
        $this->types[] = $postType;

        return $this;
    }

    public function withStatus(string $status)
    {
        $this->statuses[] = $status;

        return $this;
    }
}
