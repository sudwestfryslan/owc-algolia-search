<?php

namespace SudwestFryslan\Algolia;

class RecordServiceProvider extends ServiceProvider
{
    protected array $recordTypes = [
        'pdc-item'          => Records\PdcItem::class,
        'openpub-item'      => Records\OpenPubItem::class,
        'attachment'        => Records\Attachment::class,
        'pdc-category'      => Records\PdcTheme::class,
        'page'              => Records\PageItem::class,
        'openpub-theme'     => Records\OpenPubTheme::class,
        'construc_projects' => Records\ConstructionProject::class,
        'landing-pages'     => Records\LandingPage::class,
    ];

    public function register(): void
    {
        add_filter('algolia_post_to_record', [$this, 'castToRecord'], 10, 2);
        add_filter('algolia_post_to_record_filtered', [$this, 'applyPermalinkMethod'], 10, 2);
    }

    public function castToRecord(array $data, Post $post): array
    {
        if (! isset($this->recordTypes[$post->post_type])) {
            return $data;
        }

        $record = $this->container->get(
            $this->recordTypes[$post->post_type],
            $post
        );

        return $record->toArray();
    }

    /**
     * Update a Record' permalink if the user wants to use a custom
     * base or use relative URLs.
     * @param  array $record
     * @param  Post  $post
     * @return array
     */
    public function applyPermalinkMethod(array $record, Post $post): array
    {
        switch (get_option('algolia_permalink_method')) {
            case 'custom':
                $record['permalink'] = \str_replace(
                    home_url(),
                    get_option('algolia_custom_permalink'),
                    get_permalink($post->ID)
                );

                break;
            case 'relative':
                $record['permalink'] = \str_replace(home_url(), '', get_permalink($post->ID));

                break;
        }

        return $record;
    }
}
