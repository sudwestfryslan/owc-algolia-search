import '../scss/admin'

jQuery(document).ready(function ($) {
  $('#algolia_permalink_method').on('change', function () {
    const algoliaCpRow = $('#algolia_custom_permalink').closest('tr')

    if ($(this).val() == 'custom') {
      algoliaCpRow.show()
    } else {
      algoliaCpRow.hide()
    }
  }).trigger('change')
})

let reindexMax

function algoliaReindex (algWrap, $algAction, $algBtn) {
  // Run the ajax request
  jQuery.ajax({
    type: 'post',
    dataType: 'json',
    url: window.algolia.ajax_url,
    data: { action: $algAction },
    success: function (response) {
      if (response.status === 'success') {
        if (response.items_left > 0) {
          const $perc = (1 - (response.items_left / reindexMax)) * 100

          // Set the width and percentage
          algWrap.find('span').width($perc + '%').html(Math.round($perc) + '%')

          // Re-run the function after 400ms
          setTimeout(algoliaReindex, 400, algWrap, $algAction, $algBtn)
        } else {
          // Hide reindex wrap
          algWrap.hide()

          // Show the button
          $algBtn.show()

          // Set alert timeout
          setTimeout(function () {
            // Show alert message
            alert('Indexering afgerond')
          }, 200)
        }
      } else {
        // Hide reindex wrap
        algWrap.hide()

        // Show the button
        $algBtn.show()

        // Set alert timeout
        setTimeout(function () {
          // Show the alert
          alert(response.message)
        }, 200)
      }
    }
  })
}

jQuery(document).on('click', '#algolia-reindex--start', function (e) {
  // Prevent default action
  e.preventDefault()

  let algWrap = jQuery(this).data('wrap')
  if (algWrap !== 'undefined') {
    algWrap = jQuery(algWrap)
  } else {
    algWrap = jQuery('.algolia-reindex--status-wrap')
  }

  // Check if the wrapper is available
  if (algWrap.length > 0) {
    // Show reindex wrap
    algWrap.show()
    jQuery(this).hide()

    // Set the action and btn
    const $algAction = algWrap.data('action')
    const $algBtn = jQuery(this)

    // Run the ajax request
    jQuery.ajax({
      type: 'post',
      dataType: 'json',
      url: window.algolia.ajax_url,
      data: { action: $algAction + '_start' },
      success: function (response) {
        if (response.status === 'success') {
          // Set the max reindex
          reindexMax = response.total

          // Run the reindex
          algoliaReindex(algWrap, $algAction, $algBtn)
        } else {
          // Hide reindex wrap
          algWrap.hide()

          // Show the button
          $algBtn.show()

          // Set alert timeout
          setTimeout(function () {
            // Show the alert
            alert(response.message)
          }, 200)
        }
      },
      error: function () {
        alert('Er is een fout opgetreden tijdens de índexering')
      }
    })
  }
})
