class KeyboardNavigation {
  constructor (configuration) {
    this.$ = window.jQuery
  }

  isKeyUp (keyCode) { return keyCode === 38 }
  isKeyDown (keyCode) { return keyCode === 40 }
  isKeyUpOrDown (keyCode) { return this.isKeyUp(keyCode) || this.isKeyDown(keyCode) }
  setFocus (jqElement) { return jqElement.focus().trigger('focusin') }

  getCallback () {
    const me = this

    return function (event) {
      const keyCode = event.originalEvent.keyCode
      if (me.isKeyUpOrDown(keyCode) === false) {
        return true
      }

      const active = Object.values(window.ac).filter(inst => inst.isOpen).at(0)
      if (!active) {
        // Unable to determine the active window. Abort.
        return true
      }

      event.preventDefault()

      const el = me.$(`#${active.identifier}`)
      const resultsEl = el.find('.algolia-search-results')
      const currentFocus = resultsEl.find('.highlightable:focus')

      if (currentFocus.length <= 0) {
        // No current focus found, put the focus on the first available element.
        const highlightable = me.isKeyDown(keyCode) ? resultsEl.find('.highlightable').first() : resultsEl.find('.highlightable').last()

        return me.setFocus(highlightable)
      }

      const currentIndex = Number(currentFocus.attr('data-order'))
      if (isNaN(currentIndex)) {
        // No current index. We're either in the search bar or the footer.
        if (currentFocus.hasClass('highlightable-last') && me.isKeyDown(keyCode)) {
          // We're in the footer. Highlight the searchbar.
          return me.setFocus(el.find('.algolia-search-bar input[type=search]'))
        } else if (currentFocus.hasClass('highlightable-last') && me.isKeyUp(keyCode)) {
          // We're in the footer. Highlight the previous highlightable
          return me.setFocus(resultsEl.find('.highlightable').not('.highlightable-last').last())
        } else {
          // We're not in the footer, so we'll just highlight the first or last result.
          const highlightable = me.isKeyDown(keyCode) ? resultsEl.find('.highlightable').first() : resultsEl.find('.highlightable').last()

          return me.setFocus(highlightable)
        }
      }

      if (me.isKeyDown(keyCode)) {
        const next = resultsEl.find(`.highlightable[data-order=${currentIndex + 1}]`)
        if (!next || next.length <= 0) {
          // No higher index available, go the the footer.
          return me.setFocus(resultsEl.find('.highlightable-last'))
        }

        // Next element found. Highlight it!
        me.setFocus(next)
      } else {
        const prev = resultsEl.find(`.highlightable[data-order=${currentIndex - 1}]`)
        if (!prev || prev.length <= 0) {
          // No lower index available, go the the searchbar.
          return me.setFocus(el.find('.algolia-search-bar input[type=search]'))
        }

        // Prev element found. Highlight it!
        me.setFocus(prev)
      }
    }
  }
}

export default KeyboardNavigation
