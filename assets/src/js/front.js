import '../scss/front'

import { autocomplete, getAlgoliaResults } from '@algolia/autocomplete-js'
import { createAlgoliaInsightsPlugin } from '@algolia/autocomplete-plugin-algolia-insights'
// import { createLocalStorageRecentSearchesPlugin } from '@algolia/autocomplete-plugin-recent-searches'
import insightsClient from 'search-insights'
import KeyboardNavigation from './keyboardNavigation'

require('algoliasearch')

const snippetLength = { snippetLength: 40 }
window.ac = {}
window.ac_init = false

window.addEventListener('DOMContentLoaded', function () {
  const $ = window.jQuery

  if (!$ || typeof window.wp !== 'object' || typeof window.wp.template !== 'function') {
    return console.warn('Undefined jQuery or WP dependencies failed to load!')
  }

  // Abort if we did not find any algolia-search elements.
  const algoliaSearchElements = $('.algolia-search')
  if (algoliaSearchElements.length <= 0 || window.ac_init) {
    return
  }

  const algoliaConfig = window.algolia
  const searchClient = algoliasearch(algoliaConfig.application, algoliaConfig.api_key)

  const resultTemplate = window.wp.template('algolia-autocomplete-suggestion')
  const footerTemplate = window.wp.template('algolia-autocomplete-footer')
  const popularPagesTemplate = window.wp.template('algolia-popular-pages')

  for (let i = algoliaSearchElements.length - 1; i >= 0; i--) {
    const identifier = $(algoliaSearchElements[i]).attr('id')
    algoliaConfig.identifier = identifier

    initializeAutocomplete({
      identifier,
      algoliaConfig,
      searchClient,
      templates: { resultTemplate, footerTemplate, popularPagesTemplate },
      plugins: initializeAcPlugins(algoliaConfig)
    })

    algoliaSearchElements.show()
  }

  $(document).on('owc_algolia_get_items', updateSnippetLength)

  const keyboardNav = new KeyboardNavigation()
  $(document).on('keydown', keyboardNav.getCallback())

  $(document).on('click focusin', function (e) {
    const target = $(e.target)

    const isSearchBar = Boolean(
      target.is('.algolia-search') ||
      target.is('.algolia-search-results') ||
      target.parents('.algolia-search').length ||
      target.parents('.algolia-search-results').length
    )

    if (!isSearchBar) {
      Object.values(window.ac).forEach(inst => inst.setIsOpen(false))
    }
  })

  function initializeAcPlugins (config) {
    insightsClient('init', {
      appId: config.application,
      apiKey: config.api_key,
      region: 'de',
      useCookie: true
    })

    // Setup the 'recent searches' Algolia plugin
    // const recentSearches = createLocalStorageRecentSearchesPlugin({
    //   key: 'RECENT_SEARCH',
    //   limit: 4
    // })

    // Check if we can enable the insights plugin for Algolia
    const enableInsights = typeof algoliaConfig.client_ip === 'string' &&
      algoliaConfig.no_insight_ips.indexOf(algoliaConfig.client_ip) === -1

    if (enableInsights === false) {
      return []
    }

    // Setup the 'insights' Algolia plugin
    const algoliaInsights = createAlgoliaInsightsPlugin({
      insightsClient,
      onItemsChange ({ insights, insightsEvents, state }) {
        const events = insightsEvents.map((insightsEvent) => ({
          ...insightsEvent, eventName: 'view-page ' + state.search_identifier
        }))

        insights.viewedObjectIDs(...events)
      },
      onSelect ({ insights, insightsEvents, state }) {
        const events = insightsEvents.map((insightsEvent) => ({
          ...insightsEvent, eventName: 'page-click ' + state.search_identifier
        }))

        insights.clickedObjectIDsAfterSearch(...events)
      }
    })

    return [algoliaInsights]
  }

  function initializeAutocomplete ({
    identifier,
    algoliaConfig,
    searchClient,
    templates,
    plugins
  }) {
    // Prevent double initialisation
    if (typeof window.ac[identifier] !== 'undefined') {
      return
    }

    setContentSnippetLength(identifier, 40)
    const filters = resolveFilters(identifier)
    const placeholder = resolvePlaceholder(identifier, algoliaConfig.placeholder)

    window.ac[identifier] = autocomplete({
      container: '#' + identifier + ' .algolia-search-bar',
      panelContainer: '#' + identifier + ' .algolia-search-results',
      placeholder: placeholder,
      detachedMediaQuery: 'none',
      id: 'inst_' + identifier,
      plugins,
      openOnFocus: true,
      initialState: { query: algoliaConfig.query },
      onSubmit: function ({ state }) {
        if (state.query.length <= 0) {
          return
        }

        const url = algoliaConfig.search_url + state.query

        window.location.assign(url)
      },
      getSources ({ query }) {
        // If no query was entered, return the most popular pages.
        if (resolveDisablePopularSource(identifier) === false && (!query && !filters)) {
          return [getPopularPagesSource({ algoliaConfig, templates })]
        }

        return [getPagesSource({ searchClient, algoliaConfig, templates, identifier, query, filters })]
      },
      navigator: {
        navigate ({ itemUrl }) {
          window.location.assign(itemUrl)
        },
        navigateNewTab ({ itemUrl }) {
          const windowReference = window.open(itemUrl, '_blank', 'noopener')

          if (windowReference) {
            windowReference.focus()
          }
        },
        navigateNewWindow ({ itemUrl }) {
          window.open(itemUrl, '_blank', 'noopener')
        }
      },
      onStateChange ({ state }) {
        if (typeof window.ac[identifier] !== 'undefined') {
          window.ac[identifier].isOpen = state.isOpen
        }
      }
    })

    window.ac[identifier].identifier = identifier
  }

  function getPagesSource ({ searchClient, algoliaConfig, templates, identifier, query, filters }) {
    return {
      sourceId: 'pages',
      getItems () {
        $(document).trigger('owc_algolia_get_items', identifier)

        return getAlgoliaResults({
          searchClient,
          transformResponse ({ hits }) { return hits },
          queries: [{
            indexName: algoliaConfig.index_name,
            query: query,
            facets: ['post_type', 'themes'],
            params: {
              filters: filters,
              hitsPerPage: 4,
              clickAnalytics: true,
              highlightPreTag: '<mark class="suggestion-highlight">',
              highlightPostTag: '</mark>',
              snippetEllipsisText: '…',
              attributesToSnippet: [
                'post_content:' + getContentSnippetLength(identifier), 'keywords:5'
              ],
              attributesToRetrieve: [
                'post_title', 'post_content', 'permalink', 'post_type', 'keywords'
              ]
            }
          }]
        })
      },

      getItemUrl ({ item }) { return item.permalink },

      templates: {
        noResults ({ createElement }) { return createElement('h3', {}, 'Geen resultaten gevonden...') },
        item ({ item, createElement, state }) {
          // Assign the search identifier to the current state, so
          // that we can use it later when sending analytics events.
          state.search_identifier = identifier

          item.order_no = item.__autocomplete_id + 1

          return createElement('div', {
            dangerouslySetInnerHTML: { __html: templates.resultTemplate(item) }
          })
        },
        footer ({ state, createElement, items }) {
          if (resolveDisableFooter(identifier)) {
            return ''
          }

          return items.length <= 0
            ? ''
            : createElement('div', {
              dangerouslySetInnerHTML: { __html: templates.footerTemplate({ query: state.query }) }
            })
        }
      }
    }
  }

  function getPopularPagesSource ({ algoliaConfig, templates }) {
    return {
      sourceId: 'popular-pages',
      getItems () { return algoliaConfig.popular_pages },
      getItemUrl ({ item }) { return item.permalink },
      templates: {
        noResults () { return '' },
        item ({ item, createElement }) {
          item.order_no = item.__autocomplete_id + 1

          return createElement('div', {
            dangerouslySetInnerHTML: { __html: templates.popularPagesTemplate(item) }
          })
        },
        header ({ createElement }) { return createElement('h3', {}, "Populaire pagina's") },
        footer ({ createElement }) {
          const recentSearchesFooter = window.wp.template('algolia-popular-footer')

          return createElement('div', {
            dangerouslySetInnerHTML: { __html: recentSearchesFooter() }
          })
        }
      }
    }
  }

  function setContentSnippetLength (identifier, length) {
    snippetLength[identifier] = Number(length)
  }

  function getContentSnippetLength (identifier) {
    if (typeof snippetLength[identifier] !== 'number') {
      return 40
    }

    return snippetLength[identifier]
  }

  function updateSnippetLength (e, identifier) {
    const snippetLength = getContentSnippetLength(identifier)

    if (snippetLength !== 40) {
      return
    }

    const resultsEl = $('#' + identifier + ' .algolia-search-results')
    const resultsWidth = resultsEl.width()

    const length = calculateWordCount(resultsWidth)

    setContentSnippetLength(identifier, length)
  }

  function calculateWordCount (elementWidht) {
    const characterWidth = 7
    const avgCharactersPerWord = 7
    const nLines = 2

    return Math.floor((
      (elementWidht / characterWidth) / avgCharactersPerWord
    ) * nLines)
  }

  function resolveFilters (identifier) {
    const facets = $('#' + identifier).attr('data-facets')

    return typeof facets !== 'string' ? '' : facets
  }

  function resolvePlaceholder (identifier, defaultPlaceholder) {
    const placeholder = $('#' + identifier).attr('data-placeholder')

    return typeof placeholder !== 'string' || !placeholder ? defaultPlaceholder : placeholder
  }

  function resolveDisableFooter (identifier) {
    const disable = $('#' + identifier).attr('data-disable-footer')

    return disable === 'true'
  }

  function resolveDisablePopularSource (identifier) {
    const disable = $('#' + identifier).attr('data-disable-popular')

    return disable === 'true'
  }

  window.ac_init = true
})
